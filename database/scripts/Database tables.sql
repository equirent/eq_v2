-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server versie:                5.7.10 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Versie:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Databasestructuur van equirent wordt geschreven
CREATE DATABASE IF NOT EXISTS `equirent` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `equirent`;


-- Structuur van  tabel equirent.event wordt geschreven
CREATE TABLE IF NOT EXISTS `event` (
  `event_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `boolean_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel equirent.event: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
/*!40000 ALTER TABLE `event` ENABLE KEYS */;


-- Structuur van  tabel equirent.event_workplace wordt geschreven
CREATE TABLE IF NOT EXISTS `event_workplace` (
  `event_workplace_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fk_workplace_id` int(11) unsigned NOT NULL,
  `fk_event_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`event_workplace_id`),
  KEY `FK__workplace` (`fk_workplace_id`),
  KEY `FK__event` (`fk_event_id`),
  CONSTRAINT `FK_event_workplace_event` FOREIGN KEY (`fk_event_id`) REFERENCES `event` (`event_id`),
  CONSTRAINT `FK_event_workplace_workplace` FOREIGN KEY (`fk_workplace_id`) REFERENCES `workplace` (`workplace_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel equirent.event_workplace: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `event_workplace` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_workplace` ENABLE KEYS */;


-- Structuur van  tabel equirent.group wordt geschreven
CREATE TABLE IF NOT EXISTS `group` (
  `group_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fk_organisation_id` int(11) unsigned NOT NULL,
  `fk_event_id` int(11) unsigned NOT NULL,
  `size` int(11) NOT NULL,
  `boolean_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`),
  KEY `FK_group_organisation` (`fk_organisation_id`),
  KEY `FK_group_event` (`fk_event_id`),
  CONSTRAINT `FK_group_event` FOREIGN KEY (`fk_event_id`) REFERENCES `event` (`event_id`),
  CONSTRAINT `FK_group_organisation` FOREIGN KEY (`fk_organisation_id`) REFERENCES `organisation` (`organisation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel equirent.group: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
/*!40000 ALTER TABLE `group` ENABLE KEYS */;


-- Structuur van  tabel equirent.organisation wordt geschreven
CREATE TABLE IF NOT EXISTS `organisation` (
  `organisation_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `color` varchar(20) NOT NULL,
  `abbreviation` varchar(12) NOT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `boolean_deleted` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`organisation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel equirent.organisation: ~3 rows (ongeveer)
/*!40000 ALTER TABLE `organisation` DISABLE KEYS */;
/*!40000 ALTER TABLE `organisation` ENABLE KEYS */;


-- Structuur van  tabel equirent.person wordt geschreven
CREATE TABLE IF NOT EXISTS `person` (
  `person_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fk_person_group_id` int(11) unsigned NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `boolean_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`person_id`),
  KEY `FK_person_person_group` (`fk_person_group_id`),
  CONSTRAINT `FK_person_person_group` FOREIGN KEY (`fk_person_group_id`) REFERENCES `person_group` (`person_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel equirent.person: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
/*!40000 ALTER TABLE `person` ENABLE KEYS */;


-- Structuur van  tabel equirent.person_group wordt geschreven
CREATE TABLE IF NOT EXISTS `person_group` (
  `person_group_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fk_person_id` int(11) unsigned NOT NULL,
  `fk_group_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`person_group_id`),
  KEY `FK__person` (`fk_person_id`),
  KEY `FK__group` (`fk_group_id`),
  CONSTRAINT `FK_person_group_group` FOREIGN KEY (`fk_group_id`) REFERENCES `group` (`group_id`),
  CONSTRAINT `FK_person_group_person` FOREIGN KEY (`fk_person_id`) REFERENCES `person` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel equirent.person_group: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `person_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `person_group` ENABLE KEYS */;


-- Structuur van  tabel equirent.user wordt geschreven
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(40) DEFAULT NULL,
  `last_name` varchar(40) DEFAULT NULL,
  `auth_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel equirent.user: ~1 rows (ongeveer)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT IGNORE INTO `user` (`user_id`, `email`, `password`, `first_name`, `last_name`, `auth_token`) VALUES
	(1, 'admin', '$2y$10$vovYwRZcy7KsRMG3opzj1ebx1een/OyK7miZHefz8oqzauEXm5.Q.', 'Admin', 'de Admin', '3a0a41a0cce49ba45a37f1e316cf5226');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


-- Structuur van  tabel equirent.workplace wordt geschreven
CREATE TABLE IF NOT EXISTS `workplace` (
  `workplace_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fk_workplace_type_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `boolean_deleted` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`workplace_id`),
  KEY `FK_workplace_workplace_type` (`fk_workplace_type_id`),
  CONSTRAINT `FK_workplace_workplace_type` FOREIGN KEY (`fk_workplace_type_id`) REFERENCES `workplace_type` (`workplace_type_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel equirent.workplace: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `workplace` DISABLE KEYS */;
/*!40000 ALTER TABLE `workplace` ENABLE KEYS */;


-- Structuur van  tabel equirent.workplace_type wordt geschreven
CREATE TABLE IF NOT EXISTS `workplace_type` (
  `workplace_type_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `boolean_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`workplace_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel equirent.workplace_type: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `workplace_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `workplace_type` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;