<?php

use \core\classes\AutoLoader;
use \core\classes\config\Settings;

// Include AutoLoader
require_once __DIR__.'/core/classes/AutoLoader.php';

// Create new autoloader
new AutoLoader(__DIR__);

// Custom fatal error handling
ini_set('display_errors', false);
register_shutdown_function('shutdown_handler');

// Let the magic begin ;-)
try {
    require __DIR__.DIRECTORY_SEPARATOR.'init.php';
} catch (Exception $e) {
    if (Settings::isDevelopMode()) {
        echo
        '<pre>',
        '<h4>[EXCEPTION: ', get_class($e),']</h4>',
        '<div><span style="font-weight: bold;">Message</span></div>',
        '<div style="margin-left: 20px;">', $e->getMessage(), '</div>',
        '<div><span style="font-weight: bold;">File</span></div>',
        '<div style="margin-left: 20px;">', $e->getFile().':'.$e->getLine(), '</div>',
        '<div><span style="font-weight: bold;">Trace</span></div>',
        '<div style="margin-left: 20px;">', $e->getTraceAsString(), '</div>',
        '</pre>';
        die;
    } else {
        echo 'An unexpected error has occurred!';
        die;
    }
}

// Called when on shutdown
function shutdown_handler()
{
    if (($error = error_get_last()) && ($error['type'] & (E_ERROR | E_USER_ERROR | E_PARSE | E_CORE_ERROR |
                E_COMPILE_ERROR | E_RECOVERABLE_ERROR)))
    {
        if (Settings::isDevelopMode()) {
            echo
            '<pre>',
            '<h4>[FATAL ERROR]</h4>',
            '<div><span style="font-weight: bold;">Message</span></div>',
            '<div style="margin-left: 20px;">', $error['message'], '</div>',
            '<div><span style="font-weight: bold;">File</span></div>',
            '<div style="margin-left: 20px;">',  $error['file'].':'.$error['line'], '</div>',
            '</pre>';
            die;
        } else {
            echo 'An unexpected error has occurred!';
            die;
        }
    }
}