<?php

use \core\classes\config\Settings;
use \core\classes\controller\ControllerHandler;
use \core\exceptions\CoreException;
use \core\exceptions\ControllerException;

// Include configuration files
require __DIR__.'/config/settings.php';
require __DIR__.'/config/defines.php';

// Include controller references
require __DIR__.'/config/declarations.php';

// Generate the .htaccess with all the references
if (Settings::isDevelopMode()) {
    ControllerHandler::getInstance()->generateModRewrite();
}

// Get the controller identifier
if (isset($_GET['controller'])) {
    $controller_identifier = $_GET['controller'];
} else {
    // Use landing page controller if no controller set
    $controller_identifier = Settings::getLandingPageIdentifier();
}

// Get controller declaration
if (($controllerReference = ControllerHandler::getInstance()->getReference($controller_identifier)) == false)
{
    // Use landing page if controller declaration could not be found
    $controllerReference = ControllerHandler::getInstance()->getReference(Settings::getLandingPageIdentifier());
}

// Show message or exception that the page could not be found
// This should never happen unless the landing page is not valid
if ($controllerReference == false) {
    if (Settings::isDevelopMode()) {
        throw new CoreException(sprintf("Controller '%s' has not been declared or does not exist", $controller_identifier));
    } else {
        echo 'Page could not be found!';
        die;
    }
}

// Attempt to create the controller
try {
    $controller = $controllerReference->createController();
} catch (ControllerException $e) {
    if (Settings::isDevelopMode()) {
        throw $e;
    } else {
        echo 'Page could not be found!';
        die;
    }
}

// Set reference for convenience might need this later on
$controller->reference = $controllerReference;

// Initialize the controller
$controller->init();

// Run the controller
$controller->run();