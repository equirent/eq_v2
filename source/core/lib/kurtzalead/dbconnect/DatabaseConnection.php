<?php

namespace core\lib\kurtzalead\dbconnect;

use core\lib\kurtzalead\dbconnect\connections\IConnection;
use core\lib\kurtzalead\dbconnect\connections\MySQLiConnection;
use core\lib\kurtzalead\dbconnect\connections\PDOConnection;
use core\lib\kurtzalead\dbconnect\exceptions\connection\ConnectionException;
use core\lib\kurtzalead\dbconnect\exceptions\connection\ExecuteException;
use core\lib\kurtzalead\dbconnect\exceptions\connection\PrepareException;
use core\lib\kurtzalead\dbconnect\executables\IExecutable;

/**
 * Class DatabaseConnection
 * @package core\lib\kurtzalead\dbconnect
 */
class DatabaseConnection
{

    /* Database connection links */
    const L_MYSQLI = 1;
    const L_PDO = 2;

    /* Result types */
    const R_ASSOC = 1; /* An associative array */
    const R_NUM = 2; /* A numeric array */
    const R_BOTH = 3; /* Both associative and numeric keys */
    const R_RAW = 4; /* A raw result set */

    /** @var string */
    protected $host;

    /** @var string */
    protected $user;

    /** @var string */
    protected $password;

    /** @var string */
    protected $database;

    /** @var IConnection */
    protected $link;

    /**
     * @param $host
     * @param $user
     * @param $password
     * @param $database
     * @param int $type
     */
    public function init($host, $user, $password, $database, $type = self::L_MYSQLI)
    {

        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
        $this->database = $database;

        // Create connection
        $class = self::getConnectionClass($type);
        $this->link = new $class();

    }

    /**
     * DatabaseConnection destructor.
     */
    public function __destruct()
    {
        if ($this->link) {
            $this->link->close();
        }
    }

    /**
     * Connect to the database
     * @throws ConnectionException
     */
    public function connect()
    {
        $this->link->connect($this->host, $this->user, $this->password, $this->database);
    }

    /**
     * Disconnect from the database
     */
    public function disconnect()
    {
        $this->link->close();
    }

    /**
     * Executes a query
     * @param IExecutable $executable
     * @throws PrepareException
     * @throws ExecuteException
     */
    public function execute(IExecutable $executable)
    {
        $this->link->execute($executable->getQuery(), $executable->getParams());
    }

    /**
     * Executes a query that returns a result set
     * @param IExecutable $executable
     * @param bool|false $multi_result
     * @param int $result_type
     * @return array|\mysqli_stmt
     * @throws PrepareException
     * @throws ExecuteException
     */
    public function executeR(IExecutable $executable, $multi_result = false, $result_type = DatabaseConnection::R_ASSOC)
    {
        return $this->link->executeR($executable->getQuery(), $executable->getParams(), $multi_result, $result_type);
    }

    /**
     * @param int $type
     * @return string
     * @throws ConnectionException
     */
    protected static function getConnectionClass($type)
    {
        switch ($type) {

            case self::L_MYSQLI:
                if (extension_loaded('mysqli')) {
                    return MySQLiConnection::class;
                } else {
                    throw new ConnectionException("MySQLi extension could not be found");
                }

            case self::L_PDO:
                if (extension_loaded('pdo_mysql')) {
                    return PDOConnection::class;
                } else {
                    throw new ConnectionException("PDO extension could not be found");
                }

            default:
                throw new ConnectionException(sprintf("Integer '%s' is not a valid database connection type", $type));

        }
    }

}