<?php

namespace core\lib\kurtzalead\dbconnect\exceptions;

/**
 * Class DatabaseException
 * @package core\lib\kurtzalead\dbconnect\exceptions
 */
class DatabaseException extends \Exception {}