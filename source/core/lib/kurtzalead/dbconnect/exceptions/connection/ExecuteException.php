<?php

namespace core\lib\kurtzalead\dbconnect\exceptions\connection;

use core\lib\kurtzalead\dbconnect\exceptions\DatabaseException;

/**
 * Class ExecuteException
 * @package core\lib\kurtzalead\dbconnect\exceptions\connection
 */
class ExecuteException extends DatabaseException {}