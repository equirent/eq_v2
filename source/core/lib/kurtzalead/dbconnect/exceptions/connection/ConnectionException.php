<?php

namespace core\lib\kurtzalead\dbconnect\exceptions\connection;

use core\lib\kurtzalead\dbconnect\exceptions\DatabaseException;

/**
 * Class ConnectionException
 * @package core\lib\kurtzalead\dbconnect\exceptions\connection
 */
class ConnectionException extends DatabaseException {}