<?php

namespace core\lib\kurtzalead\dbconnect\exceptions\connection;

use core\lib\kurtzalead\dbconnect\exceptions\DatabaseException;

/**
 * Class PrepareException
 * @package core\lib\kurtzalead\dbconnect\exceptions\connection
 */
class PrepareException extends DatabaseException {}