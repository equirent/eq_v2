<?php

namespace core\lib\kurtzalead\dbconnect\connections;

use core\lib\kurtzalead\dbconnect\DatabaseConnection;
use core\lib\kurtzalead\dbconnect\exceptions\connection\ConnectionException;
use core\lib\kurtzalead\dbconnect\exceptions\connection\ExecuteException;
use core\lib\kurtzalead\dbconnect\exceptions\connection\PrepareException;

/**
 * Class mysqliConnection
 * @package core\lib\kurtzalead\dbconnect\connections
 */
class MySQLiConnection implements IConnection
{

    /** @var \mysqli */
    protected $connection;

    /**
     * @see IDatabaseConnection::connect()
     * @param string $db_host
     * @param string $db_user
     * @param string $db_pass
     * @param string $db_name
     * @throws ConnectionException
     */
    public function connect($db_host, $db_user, $db_pass, $db_name)
    {

        // Tell mysqli to throw exceptions
        \mysqli_report(MYSQLI_REPORT_STRICT);

        // Connect to the database
        try {
            $this->connection = new \mysqli($db_host, $db_user, $db_pass, $db_name);
        } catch(\mysqli_sql_exception $e) {
            throw new ConnectionException(sprintf('Database connection error: %s', $e->getMessage()));
        }

    }

    /**
     * @see IDatabaseConnection::close()
     */
    public function close()
    {
        if ($this->connection) {
            $this->connection->close();
            unset($this->connection);
        }
    }

    /**
     * @see IDatabaseConnection::execute()
     * @param $query
     * @param $params
     * @throws PrepareException
     * @throws ExecuteException
     */
    public function execute($query, $params)
    {

        // Execute query
        $statement = $this->_execute($query, $params);

        // Close statement
        $statement->close();

    }

    /**
     * @see DatabaseConnection::executeR()
     * @param string $query
     * @param array $params
     * @param bool $multi_results
     * @param int $result_type
     * @return array|\mysqli_stmt
     * @throws PrepareException
     * @throws ExecuteException
     */
    public function executeR($query, $params, $multi_results, $result_type)
    {

        // Execute query
        $statement = $this->_execute($query, $params);

        if ($statement->field_count < 1) {
            throw new ExecuteException('Expected at least one field to be returned');
        }

        // Return statement if result type is T_RAW
        if ($result_type == DatabaseConnection::R_RAW) {
            return $statement;
        }

        // mysqli might have different values
        switch ($result_type) {
            case DatabaseConnection::R_ASSOC:
                $result_type = MYSQLI_ASSOC;
                break;
            case DatabaseConnection::R_NUM:
                $result_type = MYSQLI_NUM;
                break;
            case DatabaseConnection::R_BOTH:
                $result_type = MYSQLI_BOTH;
                break;
        }

        if ($multi_results) {

            $result = array();

            // Get all results and put them in an array
            while($statement->more_results()) {
                $result[] = $statement->get_result()->fetch_all($result_type);
                $statement->next_result();
            };

        } else {
            $result = $statement->get_result()->fetch_all($result_type);
        }

        // Close statement
        $statement->close();

        return $result;
    }

    /**
     * @param $query
     * @param $params
     * @return \mysqli_stmt
     * @throws PrepareException
     * @throws ExecuteException
     */
    protected function _execute($query, $params)
    {

        // Prepare query
        $types = $this->prepareQuery($query, $params);

        // Prepare statement
        $statement = $this->prepareStatement($query, $types, $params);

        // Execute the statement
        if (!$statement->execute()) {
            throw new ExecuteException($statement->error, $statement->errno);
        }

        return $statement;
    }

    /**
     * @param $query
     * @param $params
     * @return string
     */
    protected function prepareQuery(&$query, &$params)
    {

        // For reference to types
        // http://php.net/manual/en/mysqli-stmt.bind-param.php#89544

        $types = '';

        $position = 0;
        foreach ($params as $index => $value) {

            $array = $this->prepareValue($value);

            // NULL, FALSE and TRUE are not supported by mysqli
            // You can however instead send a 1 and 0 for boolean, but not for a NULL value
            if ($array[0] === false) {

                // Find '?' that is not inside quotes
                preg_match_all('/[^"\']\?[^"\']/', $query, $matches, PREG_OFFSET_CAPTURE);

                // Replace the '?' with constant value
                $query = substr_replace($query, $array[1], $matches[0][$index][1] + 1, 1);

                // Decrease $position because one '?' is gone
                $position--;

                // Unset value and reindex the array
                unset($params[$index]);
                $params = array_values($params);

            } else {
                $types .= $array[0];
                $params[$index] = $array[1];
            }

            $position++;
        }

        return $types;
    }

    /**
     * @param array/mixed $value
     * @return array[2]
     * @throws PrepareException
     */
    protected function prepareValue($value)
    {

        if (is_array($value)) {

            $type = $value['type'];
            $value = $value['value'];

            if (is_null($value)) {
                return [false, 'NULL'];
            } elseif (is_bool($value)) {
                return [false, $value ? 'TRUE' : 'FALSE'];
            }

            switch ($type) {
                case 'blob':
                    return ['b', (string)$value];
                case 'datetime': /** @var \DateTime $value */
                    return ['s', date('Y-m-d H:i:s', $value->getTimestamp())];
                case 'float':
                case 'double':
                case 'real':
                    return ['d', (double)$value];
                case 'int':
                    if ($value > 2147483647) {
                        return ['s', (string)$value];
                    }
                    return ['i', (int)$value];
                case 'string':
                    return ['s', (string)$value];
            }

            throw new PrepareException(sprintf("Unsupported type '%s'", $type));

        } elseif (is_null($value)) {
            return [false, 'NULL'];
        } elseif (is_bool($value)) {
            return [false, $value ? 'TRUE' : 'FALSE'];
        } elseif (is_object($value)) {
            if ($value instanceof \DateTime) {
                return ['s', date('Y-m-d H:i:s', $value->getTimestamp())];
            }
        } elseif (is_string($value)) {
            return ['s', $value];
        } elseif (is_float($value) || is_double($value) || is_real($value)) {
            return ['d', (double)$value];
        } elseif (is_int($value)) {
            if ($value > 2147483647) {
                return ['s', $value];
            }
            return ['i', $value];
        }

        throw new PrepareException(sprintf("Unsupported type '%s'", gettype($value)));

    }

    /**
     * @param $query
     * @param $types
     * @param $params
     * @return \mysqli_stmt
     * @throws PrepareException
     */
    protected function prepareStatement($query, $types, $params)
    {

        // Create prepared statement
        $statement = $this->connection->prepare($query);

        // Bind params
        if ($statement != false && count($params) > 0) {
            if ($statement->bind_param($types, ...$params) == false) {
                $statement = false;
            }
        }

        // Throw exception upon failure
        if ($statement == false) {
            throw new PrepareException($this->connection->error, $this->connection->errno);
        }

        return $statement;
    }

}