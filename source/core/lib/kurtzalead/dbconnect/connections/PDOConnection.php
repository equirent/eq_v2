<?php

namespace core\lib\kurtzalead\dbconnect\connections;

use core\lib\kurtzalead\dbconnect\exceptions\connection\ConnectionException;

class PDOConnection implements IConnection
{

    /** @var \PDO */
    protected $connection;

    /**
     * @see IDatabaseConnection::connect()
     * @param string $db_host
     * @param string $db_user
     * @param string $db_pass
     * @param string $db_name
     * @throws ConnectionException
     */
    public function connect($db_host, $db_user, $db_pass, $db_name)
    {

        // Connect to the database
        try {
            $this->connection = new \PDO(sprintf('mysql:host=%s;dbname=%s;', $db_host, $db_name), $db_user, $db_pass);
        } catch (\PDOException $e) {
            throw new ConnectionException(sprintf('Database connection error: %s', $e->getMessage()));
        }

    }

    /**
     * @see IDatabaseConnection::close()
     */
    public function close()
    {
        if ($this->connection) {
            unset($this->connection);
        }
    }

    public function execute($query, $params)
    {
        // TODO: Implement execute() method.
    }

    public function executeR($query, $params, $multi_results, $result_type)
    {
        // TODO: Implement executeR() method.
    }

    public function prepare()
    {

    }

}