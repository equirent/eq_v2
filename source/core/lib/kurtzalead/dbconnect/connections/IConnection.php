<?php

namespace core\lib\kurtzalead\dbconnect\connections;

use core\lib\kurtzalead\dbconnect\exceptions\connection\ConnectionException;
use core\lib\kurtzalead\dbconnect\exceptions\connection\ExecuteException;
use core\lib\kurtzalead\dbconnect\exceptions\connection\PrepareException;

/**
 * Interface IConnection
 * @package core\lib\kurtzalead\dbconnect\connections
 */
interface IConnection
{

    /**
     * Open connection to database
     * @param string $db_host
     * @param string $db_user
     * @param string $db_pass
     * @param string $db_name
     * @throws ConnectionException
     */
    public function connect($db_host, $db_user, $db_pass, $db_name);

    /**
     * Close connection to database
     */
    public function close();

    /**
     * Executes a query
     * @param $query
     * @param $params
     * @throws PrepareException
     * @throws ExecuteException
     */
    public function execute($query, $params);

    /**
     * Executes a query that returns a result set
     * @param string $query
     * @param array $params
     * @param bool $multi_results
     * @param int $result_type
     * @return mixed
     * @throws PrepareException
     * @throws ExecuteException
     */
    public function executeR($query, $params, $multi_results, $result_type);

}