<?php

namespace core\lib\kurtzalead\dbconnect\executables;

/**
 * Interface IExecutable
 * @package core\lib\kurtzalead\dbconnect\executables
 */
interface IExecutable
{

    /**
     * @return string
     */
    public function getQuery();

    /**
     * @return array
     */
    public function getParams();

}