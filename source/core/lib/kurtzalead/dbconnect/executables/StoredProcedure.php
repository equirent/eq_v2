<?php

namespace core\lib\kurtzalead\dbconnect\executables;

/**
 * Class StoredProcedure
 * @package core\lib\kurtzalead\dbconnect\executables
 */
class StoredProcedure implements IExecutable
{

    /* @var string */
    protected $name;

    /* @var array */
    protected $params;

    /**
     * @param string $name
     * @param array $params
     */
    public function __construct($name, $params = array())
    {
        $this->name = $name;
        $this->params = $params;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        $array = array();
        for ($i = count($this->params); $i > 0; $i--) {
            $array[] = '?';
        }
        return 'CALL `'.$this->name.'`('.implode(',', $array).');';
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

}