<?php

namespace core\lib\kurtzalead\dbconnect\executables;

/**
 * Class Query
 * @package core\lib\kurtzalead\dbconnect\executables
 */
class Query implements IExecutable
{

    /** @var string */
    public $query;

    /** @var array */
    public $params;

    /**
     * DatabaseQuery constructor.
     * @param $query
     * @param $params
     */
    public function __construct($query, $params)
    {
        $this->query = $query;
        $this->params = $params;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

}