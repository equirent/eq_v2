<?php

namespace core\lib\kurtzalead\dbconnect\executables;

use core\lib\kurtzalead\dbconnect\DatabaseConnection;
use core\lib\kurtzalead\dbconnect\exceptions\connection\ExecuteException;
use core\lib\kurtzalead\dbconnect\exceptions\DatabaseException;

/**
 * Class Object
 * @package core\lib\kurtzalead\dbconnect\executables
 */
abstract class Object
{

    /**
     * @return DatabaseConnection
     */
    protected abstract function getDatabase();

    /**
     * @return array
     */
    protected abstract function &getOptions();

    /**
     * @return array
     */
    protected function &getIdentifiers()
    {
        return $this->getOptions()['identifiers'];
    }

    /**
     * @return array
     */
    protected function &getFields()
    {
        return $this->getOptions()['fields'];
    }

    /**
     * Object constructor.
     * @param ...$identifiers
     */
    public function __construct(...$identifiers)
    {

        // Load if identifier(s) given
        if (count($identifiers) > 0) {
            $this->setIdentifiers(...$identifiers);
            $this->load();
        }

    }

    /**
     * Called by autoloader
     */
    public function __init__()
    {

        // A little hack to validate if all fields are valid

        $reflectionClass = new \ReflectionClass(static::class);

        // Ignore if abstract
        if (!$reflectionClass->isAbstract()) {
            return;
        }

        if (static::class == self::class) {
            return;
        }

        $object = $reflectionClass->newInstanceWithoutConstructor();

        // Check if field options are valid
        foreach ($object->getFields() as $field_name => $options) {
            if (!isset($options['type'])) {
                throw new ObjectException(sprintf(
                    "Field options '%s' does not have a type", $field_name));
            }
        }

        // Check if identifier fields have been declared
        foreach ($fields = $object->getIdentifiers() as $field_name) {
            if (!isset($fields[$field_name])) {
                throw new ObjectException(sprintf(
                    "Identifier field options '%s' does not exist", $field_name));
            }
        }

        // Check if all fields exist in the class
        foreach ($object->getFields() as $field_name => $options) {
            if (!property_exists(static::class, $field_name)) {
                throw new ObjectException(sprintf("Field '%s' does not exist", $field_name));
            }
        }

    }

    /**
     * @param ...$identifiers
     * @throws InvalidFieldsException
     * @throws ObjectException
     */
    protected function setIdentifiers(...$identifiers)
    {

        $field_identifiers = $this->getIdentifiers();

        if (count($identifiers) != count($field_identifiers)) {
            throw new ObjectException(
                sprintf('Expected %s identifier(s) instead received a total of %s',
                    count($field_identifiers), count($identifiers)));
        }

        $exceptions = array();

        // Validate and set identifier's field value
        foreach ($identifiers as $index => $field_name) {
            try {
                $this->setFieldValue($field_name, $this->validateField($field_name, $identifiers[$index]));
            } catch (InvalidFieldException $e) {
                $exceptions[] = $e;
            }
        }

        // Throw exception with all InvalidFieldExceptions
        if (count($exceptions) != 0) {
            throw new InvalidFieldsException($exceptions);
        }

    }

    /**
     * @param $field_name
     * @return mixed
     * @throws ObjectException
     */
    protected function getFieldOptions($field_name)
    {
        return $this->getFields()[$field_name];
    }

    /**
     * @param $field_name
     * @return mixed
     * @throws ObjectException
     */
    protected function getFieldValue($field_name)
    {
        return $this->{$field_name};
    }

    /**
     * @param $field_name
     * @param $value
     * @throws ObjectException
     */
    protected function setFieldValue($field_name, $value)
    {
        $this->{$field_name} = $value;
    }

    /**
     * @param $result_or_field_name
     * @return mixed
     */
    protected function getFieldName($result_or_field_name)
    {

        $fields = $this->getFields();

        if (isset($fields[$result_or_field_name])) {
            return $result_or_field_name;
        } else {
            foreach ($fields as $field_name => $options) {
                if (isset($options['field_name']) && $options['field_name'] == $result_or_field_name) {
                    return $field_name;
                }
            }
        }

        return false;
    }

    /**
     * @param $field_name
     * @param $value
     * @return mixed
     * @throws InvalidFieldException
     */
    protected function validateField($field_name, $value)
    {

        $options = $this->getFieldOptions($field_name);

        // Check if required
        if (isset($options['required']) && $options['required'] !== false) {
            if (is_null($value)) {
                throw new InvalidFieldException(sprintf("Field '%s' can not be empty.", $field_name));
            }
        }

        // Check if field is of the correct type
        if (!static::isOfType($value, $options['type'])) {
            throw new InvalidFieldException(sprintf("Field '%s' must be of type '%s'.", $field_name, $options['type']));
        }

        if (isset($options['length'])) {
            switch ($options['type']) {

                // Check if length of the string is correct
                case 'string':
                    $length = strlen($value);
                    if ($length < $options['length'][0] || $length > $options['length'][1]) {
                        throw new InvalidFieldException(
                            sprintf("Field '%s' must not be longer than %s and not smaller than %s.",
                                $field_name, $options['length'][0], $options['length'][1]));
                    }
                    break;

                // Check if the size of the number is correct
                case 'numeric':
                case 'int':
                case 'float':
                case 'double':
                    if ($value < $options['length'][0] || $value > $options['length'][1]) {
                        throw new InvalidFieldException(
                            sprintf("Field '%s' must not be bigger than %s and not smaller than %s.",
                                $field_name, $options['length'][0], $options['length'][1]));
                    }
                    break;

            }
        }

        if (isset($options['unsigned']) && $options['unsigned'] == true) {
            switch ($options['type']) {

                // Check if the number is unsigned
                case 'numeric':
                case 'int':
                case 'float':
                case 'double':
                    if ($value < 0) {
                        throw new InvalidFieldException(sprintf("Field '%s' must not be smaller than 0.", $field_name));
                    }
                    break;

            }
        }

        return $value;
    }

    /**
     * Whether this object's identifier(s) are valid
     * @return bool
     */
    public function isValid()
    {
        try {
            foreach ($this->getIdentifiers() as $index => $field_name) {
                $this->validateField($field_name, $this->getFieldValue($field_name));
            }
        } catch (InvalidFieldException $e) {
            return false;
        }
        return true;
    }

    /**
     * @param $values
     * @return array
     * @throws InvalidFieldsException
     */
    protected function prepareFields($values)
    {

        $field_values = array();
        $exceptions = array();

        // Validate and set identifier's field value
        foreach ($values as $value) {
            if (is_array($value)) {
                try {
                    $field_values[] = $this->validateField($value[0], $this->getFieldValue($value[0]));
                } catch (InvalidFieldException $e) {
                    $field_warnings[] = $e;
                }
            } else {
                $exceptions[] = $value;
            }
        }

        // Throw warnings exception, controller can catch this
        if (count($exceptions) != 0) {
            throw new InvalidFieldsException($exceptions);
        }

        return $field_values;
    }

    /**
     * @param $name
     * @return mixed
     * @throws ObjectException
     */
    protected function getExecutable($name)
    {

        // Check if function exists
        if (!\method_exists($this, 'get' . $name . 'Executable')) {
            throw new ObjectException(
                sprintf("Attempted to get procedure options, but function '%s' does not exist",
                    'get' . $name . 'Executable'));
        }

        // Get executable
        $executable = call_user_func(array($this, 'get' . $name . 'Executable'));

        // Throw exception is executable is invalid
        if (!$executable instanceof IExecutable) {
            throw new ObjectException('Invalid executable');
        }

        return $executable;
    }

    /**
     * @param $name
     * @throws ObjectException
     */
    protected function call($name)
    {
        $this->getDatabase()->execute($this->getExecutable($name));
    }

    /**
     * @param $name
     * @param bool|false $multi_result
     * @param int $result_type
     * @return array|\mysqli_stmt
     * @throws ObjectException
     */
    protected function callR($name, $multi_result = false, $result_type = DatabaseConnection::R_ASSOC)
    {
        return $this->getDatabase()->executeR($this->getExecutable($name), $multi_result, $result_type);
    }

    /**
     * @throws ObjectException
     */
    public function load()
    {

        if (!$this->isValid()) {
            throw new ObjectException('Object is not valid');
        }

        try {
            $result = $this->callR('Load');
        } catch (ExecuteException $e) {
            throw new ObjectException(sprintf('Object could not be loaded: %s', $e->getMessage()));
        }

        // Populate fields
        foreach ($result[0] as $result_name => $value) {
            $this->setFieldValue($this->getFieldName($result_name), $value);
        }

    }

    /**
     * @return mixed
     */
    protected abstract function getLoadExecutable();

    /**
     * Inserts object in to the database
     * @throws ObjectException
     */
    public function add()
    {

        $result = $this->callR('Add');

        foreach ($this->getIdentifiers() as $field_name) {
            if (!isset($result[0][$field_name])) {
                throw new ObjectException(sprintf("Expected field identifier '%s' to be returned", $field_name));
            }
        }

        // Populare identifier(s)'s field(s)
        foreach ($result[0] as $result_name => $value) {
            $this->setFieldValue($this->getFieldName($result_name), $value);
        }

    }

    /**
     * Updates object in the database
     * @throws ObjectException
     */
    public function update()
    {
        $this->call('Update');
    }

    /**
     * Whether the given value is of the given type
     * @param $value
     * @param $type
     * @return bool
     */
    protected static function isOfType($value, $type)
    {
        switch ($type) {
            case 'null':
                return is_null($value);
            case 'bool':
                return is_bool($value);
            case 'numeric':
                return is_numeric($value);
            case 'int':
                return is_int($value);
            case 'float':
                return is_float($value);
            case 'double':
                return is_double($value);
            case 'string':
                return is_string($value);
            case 'array':
                return is_array($value);
            case 'object':
                return is_object($value);
        }
        return false;
    }

}

/**
 * Class ObjectException
 * @package Core\Database
 */
class ObjectException extends DatabaseException {}

/**
 * Class InvalidFieldException
 * @package Core\Database
 */
class InvalidFieldException extends ObjectException {}

/**
 * Class InvalidFieldsException
 * @package core\lib\kurtzalead\dbconnect\executables
 */
class InvalidFieldsException extends ObjectException
{

    /**
     * @var InvalidFieldException[]
     */
    protected $exceptions = array();

    /**
     * InvalidFieldsException constructor.
     * @param InvalidFieldException[] $exceptions
     */
    public function __construct($exceptions)
    {
        parent::__construct();
    }

    /**
     * @return InvalidFieldException[]
     */
    public function getExceptions()
    {
        return $this->exceptions;
    }

}