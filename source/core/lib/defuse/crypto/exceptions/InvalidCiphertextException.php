<?php
namespace core\lib\defuse\crypto\exceptions;

class InvalidCiphertextException extends CryptoException {}