<?php

namespace core\lib\defuse\crypto\exceptions;

class CannotPerformOperationException extends CryptoException {}