<?php
namespace core\lib\defuse\crypto\exceptions;

class CryptoTestFailedException extends CryptoException {}