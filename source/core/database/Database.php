<?php

namespace core\database;

use core\classes\config\Settings;
use core\lib\kurtzalead\dbconnect\DatabaseConnection;

/**
 * Class Database
 * @package core\database
 */
class Database extends DatabaseConnection
{

    /** @var Database */
    private static $instance;

    /**
     * Database constructor.
     */
    private function __construct()
    {
        $info = Settings::getDatabaseConnectionInfo();
        $this->init(
            $info['host'],
            $info['user'],
            $info['password'],
            $info['name']
        );
        $this->connect();
    }

    /**
     * @return Database
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}