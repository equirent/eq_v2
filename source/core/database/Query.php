<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 3-3-2016
 * Time: 11:38
 */

namespace core\database;

use core\lib\kurtzalead\dbconnect\executables\Query as DatabaseQuery;

/**
 * Class Query
 * @package core\database
 */
class Query extends DatabaseQuery {}