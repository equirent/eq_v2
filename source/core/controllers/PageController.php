<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 21-2-2016
 * Time: 12:47
 */

namespace core\controllers;
use core\classes\config\Defines;
use core\classes\config\Settings;
use core\classes\controller\ControllerCore;
use core\classes\controller\ControllerReference;
use core\classes\Template;
use core\classes\Tools;
use core\exceptions\CoreException;

/**
 * Class PageController
 * @package core\controllers
 */
abstract class PageController extends ControllerCore
{

    /** @var bool */
    protected $include_jquery = true;

    /** @var bool */
    protected $include_angular = true;

    /** @var bool */
    protected $include_bootstrap = true;

    /** @var string The requested function name */
    protected $request_method = false;

    /** @var string */
    protected $title;

    /** @var  array All CSS files that will be included in the header of the page. MUST be a full URL. */
    protected $css_files = array();

    /** @var array All CSS files that will be included in the header of the page. MUST be a full URL. */
    protected $js_files = array();

    /** @var array All CSS files that will be included in the footer of the page. MUST be a full URL. */
    protected $js_files_foot = array();

    /**
     * Run the controller
     */
    public function run()
    {

        // Initializes the request
        $this->initRequest();

        // Process the request before we have access
        $this->preProcessRequest();

        // Check if we got access to the controller
        if ($this->hasAccess() == false) {
            $this->onAccessDenied();
            return;
        }

        /*
         * At this point we have access to the controller
         */

        // Initialize the controller
        $this->postInit();

        // Processes the request
        $this->processRequest();

        // Process for the controller itself
        $this->process();

        /*
         * At this point all processing for the controller has been done
         * You MAY not process in the display e.g. call database
         */

        // Set media resources
        $this->setMedia();

        // Display everything
        $this->display();

    }

    /**
     * Whether we have access to the controller
     * @return bool
     */
    protected function hasAccess()
    {
        return true;
    }

    /**
     * Called when hasAccess returns false
     */
    protected function onAccessDenied() {}

    /**
     * Initialize the controller
     * Called after checking access
     */
    protected function postInit()
    {
        // Do whatever you like
    }

    /**
     * Overall processing for the controller
     */
    protected function process()
    {
        // Do whatever you like as long as it has to do with processing stuff ;-)
    }

    /**
     * Initializes request
     */
    protected function initRequest()
    {

        // Set request method
        $method = $this->getValue('method', false);
        if (!empty($method)) {
            $this->request_method = $method;
        }

    }

    /**
     * @return bool|mixed
     */
    protected function preProcessRequest()
    {
        if ($this->request_method) {
            return $this->callRequestMethod($this->request_method, 'pre_');
        }
        return false;
    }

    /**
     * @return bool|mixed
     */
    protected function processRequest()
    {
        if ($this->request_method) {
            return $this->callRequestMethod($this->request_method, '');
        }
        return false;
    }

    /**
     * @param string $method_name
     * @param string $prefix
     * @return mixed|bool
     */
    protected function callRequestMethod($method_name, $prefix = '')
    {

        $function_name = 'process_'.$prefix.$method_name;

        // Return false if method does not exists
        if (!method_exists($this, $function_name)) {
            return false;
        }

        // Call the requested method
        return $this->{$function_name}();
    }

    /**
     * Set media
     * CSS and JavaScript files etc...
     */
    protected function setMedia()
    {

        if ($this->include_jquery) {
            $this->addJS('resources/js/jquery.min.js', false);
        }

        if ($this->include_angular) {
            $this->addJS('resources/js/angular.min.js', false);
        }

        $this->addCSS('resources/css/global.css');
        $this->addJS('resources/js/global.js', true);

        if ($this->include_bootstrap) {
            $this->addCSS('resources/css/bootstrap.min.css');
            $this->addCSS('resources/css/bootstrap-theme.min.css'); // TODO: Eventueel een ander bootstrap thema bijvoorbeeld: https://bootswatch.com/
            $this->addCSS('resources/css/font-awesome.min.css');
            $this->addJS('resources/js/bootstrap.min.js', true);
            if ($this->include_angular) {
                $this->addJS('resources/js/angular-ui-bootstrap-tpls.min.js', false);
            }
        }

    }

    /**
     * Display the whole page
     */
    protected final function display()
    {
        $this->displayHeader();
        $this->displayContent();
        $this->displayFooter();
    }

    /**
     * Display the head
     */
    protected function displayHeader()
    {
        $this->createTemplate('header.php', array(
            'title' => ($this->title != '' ? ($this->title.' | ') : '').'eQuiRent', // TODO: Move eQuiRent to config
            'css_files' => $this->css_files,
            'js_files' => $this->js_files
        ))->show();
    }

    /**
     * Display content
     */
    protected abstract function displayContent();

    /**
     * Display the footer
     */
    protected function displayFooter()
    {
        $this->createTemplate('footer.php', array(
            'js_files' => $this->js_files_foot
        ))->show();
    }

    /**
     * Adds a CSS file that will be included with the page.
     * @param string $file Relative to the content/view/ directory
     * @return string
     */
    protected function addCSS($file)
    {
        $this->css_files[] = Tools::formatUrl(Defines::getRootDirUrl().'/content/view/'.$file);
    }

    /**
     * Adds a JS file that will be included with the page.
     * @param string $file Relative to the content/view/ directory
     * @param bool|false $footer Whether the JS file should be included in the footer instead of the header
     */
    protected function addJS($file, $footer = false)
    {
        if ($footer) {
            $this->js_files_foot[] = Tools::formatUrl(Defines::getRootDirUrl().'/content/view/'.$file);
        } else {
            $this->js_files[] = Tools::formatUrl(Defines::getRootDirUrl().'/content/view/'.$file);
        }
    }

    /**
     * Create a new template file
     * @param $file
     * @param $vars
     * @return Template
     */
    protected function createTemplate($file, $vars = array())
    {
        return new Template(Tools::formatPath(Defines::getRootDir().'/content/view/'.$file), $vars, true);
    }

    /**
     * Create a new template string
     * @param $content
     * @param $vars
     * @return Template
     */
    protected function createTemplateString($content, $vars = array())
    {
        return new Template($content, $vars, false);
    }

}