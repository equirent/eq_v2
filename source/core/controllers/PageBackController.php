<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 18-2-2016
 * Time: 11:17
 */

namespace core\controllers;

use content\controllers\front\Login;
use core\classes\controller\ControllerHandler;
use core\classes\Tools;
use core\classes\User;

/**
 * Class PageBackController
 * @package core\controllers
 */
abstract class PageBackController extends PageController
{

    /** @var User */
    protected $user;

    /**
     * @return array
     */
    protected function getNavigationUrls()
    {
        return array(
            array(
                'name' => 'Werkplekken',
                'url' => $this->getReference('a-workplace')->buildUrl()
            ),
            array(
                'name' => 'Organisaties',
                'url' => $this->getReference('a-organisation')->buildUrl()
            )
        );
    }

    /**
     * Whether we have access to the controller
     * @return bool
     */
    protected function hasAccess()
    {

        // Get the remembered user
        $this->user = User::getRemembered();

        // Access if the user is loaded
        return $this->user != false;
    }

    /**
     * Called when hasAccess returns false
     */
    protected function onAccessDenied()
    {

        // Redirect to the login controller
        $this->redirect($this->getReference('login'), array(
            'error' => Login::ERROR_PERMISSIONS_DENIED
        ), array(
            'ref' => $this->reference->getIdentifier()
        ));

    }

    /**
     *
     */
    protected function setMedia()
    {
        parent::setMedia();
        $this->addCSS('back/resources/css/global.css');
    }

    /**
     * Display the header
     */
    protected function displayHeader()
    {
        parent::displayHeader();
        $this->displayHeaderContent();
    }

    /**
     * Display the header content
     */
    protected function displayHeaderContent()
    {

        $home_url = $this->getReference('a-home')->buildUrl();

        // Build logout url
        $logout_url = $this->getReference('login')->buildUrl();
        $logout_url = Tools::appendQueryString($logout_url, array(
            'method' => 'logout'
        ));

        // Get all navigation URLs
        $nav_urls = $this->getNavigationUrls();

        // Display navigation template
        $this->createTemplate('back/navigation.php', array(
            'user_name' => $this->user->first_name.' '.$this->user->last_name,
            'home_url' => $home_url,
            'logout_url' => $logout_url,
            'nav_urls' => $nav_urls,
        ))->show();

    }

    /**
     * Display the footer content
     */
    protected function displayFooterContent()
    {
        $this->createTemplate('back/footer.php', array(
            // TODO: Assign variables to this template
        ))->show();
    }

    /**
     * Display the footer
     */
    protected function displayFooter()
    {
        $this->displayFooterContent();
        parent::displayFooter();
    }

}