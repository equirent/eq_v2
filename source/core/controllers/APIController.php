<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 29-2-2016
 * Time: 09:07
 */

namespace core\controllers;

use core\classes\controller\ControllerCore;
use core\classes\JSONResponse;
use core\classes\User;
use core\exceptions\CoreException;
use core\exceptions\InvalidValueException;
use core\exceptions\MultiException;

/**
 * Class APIController
 * @package core\controllers
 */
class APIController extends ControllerCore
{

    /** @var User */
    protected $user;

    /** @var JSONResponse */
    protected $response;

    /** @var string */
    protected $request_method;

    /**
     * Run the controller
     */
    public function run()
    {

        // Initialize the response as a first
        $this->initResponse();

        try {

            // Initializes the request
            $this->initRequest();

            // Overall controller processing
            $this->preProcess();

            // Process the request before we have access
            $this->preProcessRequest();

            // Check if we got access to the controller
            if ($this->hasAccess() == false) {
                $this->onAccessDenied();
                return;
            }

            // Overall controller processing
            $this->process();

            // Processes the request
            $this->processRequest();

        } catch (MultiException $e) {

            $this->response->setStatus(JSONResponse::STATUS_ERROR);
            foreach ($e->getExceptions() as $exception) {
                $this->response->addError($exception->getMessage());
            }

        } catch (\Exception $e) {

            // Error on exception
            $this->response->setStatus(JSONResponse::STATUS_ERROR);
            $this->response->addError($e->getMessage());

        }

        // Display the result
        $this->displayResult();

    }

    /**
     * @param array $data
     */
    protected function setData($data)
    {
        $this->response->setData($data);
    }

    /**
     * @param string $status JSONResponse::STATUS_*
     */
    protected function setStatus($status)
    {
        $this->response->setStatus($status, true);
    }

    /**
     *
     */
    protected function initResponse()
    {
        $this->response = new JSONResponse();
    }

    /**
     *
     */
    protected function initRequest()
    {

        // Set request method
        $method = $this->getValue('method', false);

        // Throw exception if method was not given
        if (empty($method)) {
            throw new InvalidValueException('No method given.');
        }

        $this->request_method = $method;

    }

    /**
     * Whether we have access to the controller
     * @return bool
     */
    protected function hasAccess()
    {

        // Get the remembered user
        $this->user = User::getRemembered();

        // Return whether the user object was loaded
        return $this->user != false;
    }

    /**
     * Called when hasAccess returns false
     */
    protected function onAccessDenied()
    {

        // Return Permissions denied error
        $this->response->setStatus(JSONResponse::STATUS_ERROR);
        $this->response->addError('Permissions denied.');

        // Display the result
        $this->displayResult();

    }

    /**
     * Overall controller processing
     */
    protected function preProcess() {}

    /**
     * Overall controller processing
     */
    protected function process() {}

    /**
     * @return bool|mixed
     */
    protected function preProcessRequest()
    {
        if ($this->request_method) {
            return $this->callRequestMethod($this->request_method, 'pre_');
        }
        return false;
    }

    /**
     * @return bool|mixed
     */
    protected function processRequest()
    {
        if ($this->request_method) {
            return $this->callRequestMethod($this->request_method, '');
        }
        return false;
    }

    /**
     * @param string $method_name
     * @param string $prefix
     * @return mixed|bool
     */
    protected function callRequestMethod($method_name, $prefix = '')
    {

        $function_name = 'process_'.$prefix.$method_name;

        // Return false if method does not exists
        if (!method_exists($this, $function_name)) {
            return false;
        }

        // Call the requested method
        return $this->{$function_name}();
    }

    /**
     * Displays the result
     */
    protected final function displayResult()
    {

        // Set the header
        header('Content-Type: application/json');

        // Set the status code
        $this->statusCode($this->response->getStatusCode());

        // echo the JSON result
        echo $this->response->buildResult();

    }

}