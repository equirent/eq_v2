<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 18-2-2016
 * Time: 11:17
 */

namespace core\controllers;

/**
 * Class PageFrontController
 * @package core\controllers
 */
abstract class PageFrontController extends PageController
{

    /**
     *
     */
    protected function setMedia()
    {
        parent::setMedia();
        $this->addCSS('front/resources/css/global.css');
    }

    /**
     * Display the header
     */
    protected function displayHeader()
    {
        parent::displayHeader();
        $this->displayHeaderContent();
    }

    /**
     * Display the header content
     */
    protected function displayHeaderContent()
    {
        $this->createTemplate('front/navigation.php', array(
            // TODO: Assign variables to this template
        ))->show();
    }

    /**
     * Display the footer content
     */
    protected function displayFooterContent()
    {
        $this->createTemplate('front/footer.php', array(
            // TODO: Assign variables to this template
        ))->show();
    }

    /**
     * Display the footer
     */
    protected function displayFooter()
    {
        $this->displayFooterContent();
        parent::displayFooter();
    }

}