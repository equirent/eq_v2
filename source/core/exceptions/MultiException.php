<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 11-3-2016
 * Time: 10:25
 */

namespace core\exceptions;

/**
 * Class MultiException
 * @package core\exceptions
 */
class MultiException extends CoreException
{

    /** @var CoreException[] */
    protected $exceptions = array();

    /**
     * MultiException constructor.
     * @param CoreException[] $exceptions
     */
    public function __construct($exceptions = array())
    {
        $this->exceptions = $exceptions;
    }

    /**
     * @param $exception
     */
    public function addException($exception)
    {
        $this->exceptions[] = $exception;
    }

    /**
     * @return CoreException[]
     */
    public function getExceptions()
    {
        return $this->exceptions;
    }

    /**
     * @return bool
     */
    public function hasExceptions()
    {
        return count($this->exceptions) > 0;
    }

}