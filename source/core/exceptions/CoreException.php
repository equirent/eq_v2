<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 15-12-2015
 * Time: 11:10
 */

namespace core\exceptions;

/**
 * Our own default exception
 * Class CoreException
 * @package core\exceptions
 */
class CoreException extends \Exception {}