<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 1-3-2016
 * Time: 11:47
 */

namespace core\exceptions;

/**
 * Class ResponseException
 * @package core\exceptions
 */
class ResponseException extends CoreException {}