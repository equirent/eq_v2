<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 18-2-2016
 * Time: 09:43
 */

namespace core\exceptions;

/**
 * Class ControllerException
 * @package core\exceptions
 */
class ControllerException extends CoreException {}