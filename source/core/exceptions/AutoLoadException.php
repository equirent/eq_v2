<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 6-2-2016
 * Time: 19:56
 */

namespace core\exceptions;

/**
 * Class AutoLoadException
 * @package core\exceptions
 */
class AutoLoadException extends CoreException {}