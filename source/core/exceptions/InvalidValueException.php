<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 11-3-2016
 * Time: 10:04
 */

namespace core\exceptions;

/**
 * Class InvalidValueException
 * @package core\exceptions
 */
class InvalidValueException extends CoreException {}