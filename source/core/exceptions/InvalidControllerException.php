<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 29-2-2016
 * Time: 13:28
 */

namespace core\exceptions;

/**
 * Class InvalidControllerException
 * @package core\exceptions
 */
class InvalidControllerException extends CoreException {}