<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 3-3-2016
 * Time: 09:02
 */

namespace core\exceptions;

/**
 * Class EncryptionException
 * @package core\exceptions
 */
class EncryptionException extends CoreException {}