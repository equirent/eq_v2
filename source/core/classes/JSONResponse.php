<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 24-2-2016
 * Time: 15:45
 */

namespace core\classes;

use core\classes\controller\ControllerCore;
use core\exceptions\ResponseException;

/**
 * Class JSONResponse
 * @package core\classes
 */
class JSONResponse
{

    /**
     * All went well, and (usually) some data was returned.
     * Keys: status, data
     */
    const STATUS_SUCCESS = 1;

    /**
     * There was a problem with the data submitted, or some pre-condition of the API call wasn't satisfied
     * Keys: status, data, errors
     */
    const STATUS_FAIL = 2;

    /**
     * An error occurred in processing the request, i.e. an exception was thrown
     * Keys: status, errors
     */
    const STATUS_ERROR = 3;

    /** @var string */
    protected $status = self::STATUS_SUCCESS;

    /** @var bool */
    protected $status_code = 200;

    /** @var array */
    protected $data = array();

    /** @var array */
    protected $errors = array();

    /**
     * Set the status for the response. Depending on the status it will return data and/or errors
     * One of the following constants are allowed:
     *  STATUS_SUCCESS, STATUS_FAIL, STATUS_ERROR
     * @param string $status
     * @param bool $replace_code
     */
    public function setStatus($status, $replace_code = true)
    {
        $this->status = $status;
        if ($replace_code) {
            switch ($this->status) {
                case self::STATUS_SUCCESS:
                    $this->status_code = 200;
                    break;
                case self::STATUS_FAIL:
                case self::STATUS_ERROR:
                default:
                    $this->status_code = 400;
            }
        }
    }

    /**
     * Get the status of the response.
     * One of the following constants will be returned:
     *  STATUS_SUCCESS, STATUS_FAIL, STATUS_ERROR
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status_code
     */
    public function setStatusCode($status_code)
    {
        $this->status_code = $status_code;
    }

    /**
     * @return bool
     */
    public function getStatusCode()
    {
        return $this->status_code;
    }

    /**
     * @param $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Get reference to the data array
     * @return array
     */
    public function &getData()
    {
        return $this->data;
    }

    /**
     * Set all errors
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Adds a single error
     * @param $message
     */
    public function addError($message)
    {
        $this->errors[] = $message;
    }

    /**
     * Get reference to the error array
     * @return array
     */
    public function &getErrors()
    {
        return $this->errors;
    }

    /**
     * Builds the JSON string and returns it
     * @return string
     * @throws ResponseException
     */
    public function buildResult()
    {

        $array = array(
            'status' => '',
            'code' => $this->status_code,
        );

        switch ($this->status) {
            case self::STATUS_SUCCESS:
                $array['status'] = 'success';
                $array['data'] = $this->data;
                break;
            case self::STATUS_ERROR:
                $array['status'] = 'error';
                $array['errors'] = $this->errors;
                break;
            case self::STATUS_FAIL:
                $array['status'] = 'fail';
                $array['data'] = $this->data;
                $array['errors'] = $this->errors;
                break;
            default:
                $array['errors'] = $this->errors;
                $array['errors'][] = sprintf("Invalid response '%s'", $this->status);
        }

        return json_encode($array);
    }

}