<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 17-2-2016
 * Time: 10:54
 */

namespace core\classes\controller;

use core\classes\config\Defines;
use core\classes\config\Settings;
use core\classes\Tools;
use core\exceptions\AutoLoadException;
use core\exceptions\ControllerException;
use core\exceptions\CoreException;

/**
 * A controller reference, refers to a controller class
 * Usually used to redirect the page
 * Class ControllerReference
 * @package core\classes
 */
final class ControllerReference
{

    /** @var ControllerDeclaration */
    private $declaration;

    /**
     * ControllerReference constructor.
     * @param ControllerDeclaration $declaration
     */
    public function __construct(ControllerDeclaration $declaration)
    {
        $this->declaration = $declaration;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->declaration->getIdentifier();
    }

    /**
     * Get the full class of the controller
     * @return string
     */
    public function getClass()
    {
        return 'content\\controllers\\'.$this->declaration->getClass();
    }

    /**
     * Build URL for the reference
     * @param array $args
     * @return mixed
     * @throws CoreException
     */
    public function buildUrl(...$args)
    {
        if (Settings::isSEOUrls()) {

            $url = call_user_func($this->declaration->getUrlBuilderCallable(), $args);

            // Throw exception if no string returned
            if (!$url || !is_string($url)) {
                throw new CoreException('SEO URL builder must return an URL relative to the root.');
            }

            return Tools::formatUrl(Defines::getRootDirUrl().'/'.$url);
        } else {
            return Tools::formatUrl(Tools::appendQueryString(Defines::getRootDirUrl().'/', array(
                'controller' => $this->declaration->getIdentifier()
            )));
        }
    }

    /**
     * Creates an instance of the controller
     * @param array ...$args
     * @return mixed
     * @throws ControllerException
     */
    public function createController(...$args)
    {

        // Get the controller's class
        $class = $this->getClass();

        // Create new instance of $class
        try {
            $controller = new $class(...$args);
        } catch (AutoLoadException $e) {
            // Throw exception if class does not exist
            throw new ControllerException($e->getMessage());
        }

        // Throw exception if the controller is not a child of ControllerCore
        if (!($controller instanceof ControllerCore)) {
            throw new ControllerException(sprintf("Controller class '%s' is not a child of '%s'",
                $class, ControllerCore::class));
        }

        return $controller;
    }

}