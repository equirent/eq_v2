<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 29-2-2016
 * Time: 09:12
 */

namespace core\classes\controller;

use core\exceptions\CoreException;

/**
 * Class ControllerDeclaration
 * @package core\classes\controller
 */
final class ControllerDeclaration
{

    /** @var string */
    protected $identifier;

    /** @var string Class of the controller relative to content\controller\ */
    protected $class;

    /** @var callable */
    protected $url_builder_callable;

    /**
     * ControllerDeclaration constructor.
     * @param string $identifier
     * @param string $class
     * @param callable $url_builder_callable
     */
    public function __construct($identifier, $class, $url_builder_callable)
    {
        $this->identifier = $identifier;
        $this->class = $class;
        $this->url_builder_callable = $url_builder_callable;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @return callable
     */
    public function getUrlBuilderCallable()
    {
        return $this->url_builder_callable;
    }

}