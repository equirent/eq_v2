<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 17-2-2016
 * Time: 08:59
 */

namespace core\classes\controller;

use core\classes\config\Settings;
use core\classes\Tools;
use core\exceptions\CoreException;
use core\exceptions\InvalidValueException;
use core\exceptions\MultiException;

/**
 * Class ControllerCore
 * @package core\classes\controller
 */
abstract class ControllerCore
{

    /** @var ControllerReference */
    public $reference;

    /** @var ControllerSession */
    public $session;

    /**
     * HTTP response status codes
     * @var array
     */
    protected static $STATUS_CODES = array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Time-out',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Large',
        415 => 'Unsupported Media Type',
        416 => 'Requested range not satisfiable',
        417 => 'Expectation Failed',
        429 => 'Too Many Requests',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Time-out',
        505 => 'Unsupported Version'
    );

    /**
     * Controller constructor.
     * If overridden, parent::__construct() must be called
     * $this->session has not been set at this point
     */
    public function __construct() {}

    /**
     * Called before running the controller
     * Secondary constructor
     * If overridden, parent::init() must be called
     */
    public function init()
    {

        // Set the controller's session
        $this->session = new ControllerSession($this->reference);
        $this->session->load();
        $this->session->clear(); // Clear $_SESSION['controller'] values

    }


    /**
     * Run the controller
     */
    abstract function run();

    /**
     * Redirect to the same controller
     * @param array $session_args
     * @param array $query_args
     * @param array ...$args
     * @throws CoreException
     */
    protected function refresh($session_args = array(), $query_args = array(), ...$args)
    {
        $this->redirect($this->reference, $session_args, $query_args, $args);
    }

    /**
     * Redirects to another controller
     * @param ControllerReference $reference
     * @param array $session_args
     * @param array $query_args
     * @param array ...$args
     * @throws CoreException
     */
    protected function redirect(ControllerReference $reference, $session_args = array(), $query_args = array(),...$args)
    {

        // Set the new session values
        $session = new ControllerSession($reference);
        $session->setValues($session_args);

        // Save the session
        $session->save();

        // Build the URL for the reference
        $url = $reference->buildUrl($args);

        // Append query string
        $url = Tools::appendQueryString($url, $query_args);

        header('Location: '.$url);
        echo '<script>location.href = "', $url, '";</script>',
            '<span>Oops, something went wrong while redirecting the page!</span>',
            '<br/><span>If the page does not refresh automatically, ',
            '<a href="'.$url.'">click here</a> to refresh manually.</span>';

        die;

//        if (Tools::getGetValue('ref', false) == '1') {
//            if (Settings::isDevelopMode()) {
//                throw new CoreException('Attempted to refresh a page twice, '
//                    .'resolve this error to prevent a refresh loop');
//            } else {
//                $url = $reference->buildUrl($query_strings, ...$args);
//                echo '<span>Oops, something went wrong while redirecting the page!</span>',
//                '<br/><span>To prevent the page from looping, ',
//                '<a href="'.$url.'">click here</a> to refresh manually.</span>';
//            }
//        } else {
////            $query_strings['ref'] = '1';
//            $url = $reference->buildUrl($query_strings, ...$args);
//            header('Location: '.$url);
//            echo '<script>location.href = "'.$url.'";</script>',
//            '<span>Oops, something went wrong while redirecting the page!</span>',
//            '<br/><span>If the page does not refresh automatically, ',
//            '<a href="'.$url.'">click here</a> to refresh manually.</span>';
//        }
//        die;
    }

    /**
     * Set the response status code
     * @param int $code
     * @param bool $replace Whether it should replace the current status code, if already set
     */
    protected function statusCode($code, $replace = false)
    {
        header(sprintf('HTTP/1.1 %d %s', (int)$code, self::$STATUS_CODES[$code]), $replace);
    }

    /**
     * @param $url
     */
    protected function location($url)
    {
        header('Location: '.$url);
    }

    /**
     * @param $key
     * @param bool|false $default_value
     * @param bool|false $throw_exception
     * @return bool
     * @throws InvalidValueException
     */
    protected function getAngularValue($key, $default_value = false, $throw_exception = false)
    {
        static $params;
            if (!$params) {
            $params = json_decode(file_get_contents('php://input'),true);
        }
        if (!isset($params[$key])) {
            if ($throw_exception) {
                throw new InvalidValueException(sprintf("Value for '%s' could not be found.", $key));
            }
            return false;
        }
        return $params[$key];
    }

    protected function getAngularValues($keys, $throw_exception = false)
    {

        $values = array();
        $exceptions = new MultiException();

        foreach ($keys as $key => $default_value) {
            try {
                $values[$key] = $this->getAngularValue($key, $default_value ?: false, $throw_exception);
            } catch (InvalidValueException $e) {
                $exceptions->addException($e);
            }
        }

        if ($exceptions->hasExceptions()) {
            throw $exceptions;
        }

        return $values;
    }

    /**
     * Get a value
     * Priority goes as follows:
     *  session -> post -> get
     * @param string $key
     * @param bool|false $default_value
     * @param bool|false $throw_exception
     * @return mixed
     * @throws InvalidValueException
     */
    protected function getValue($key, $default_value = false, $throw_exception = false)
    {
        try {
            return $this->getAngularValue($key, $default_value, true);
        } catch (InvalidValueException $e) {
            $session_values = $this->session->getValues();
            if (isset($session_values[$key])) {
                return $session_values[$key];
            }
            return $this->getRequestValue($key, $default_value, $throw_exception);
        }
    }

    /**
     * Get multiple values at once
     * Priority goes as follows:
     *  session -> post -> get
     * @param array $keys
     * @param bool|false $throw_exception
     * @return mixed
     * @throws MultiException
     */
    protected function getValues($keys, $throw_exception = false)
    {

        $values = array();
        $exceptions = new MultiException();

        foreach ($keys as $key => $default_value) {
            try {
                $values[$key] = $this->getValue($key, $default_value ?: false, $throw_exception);
            } catch (InvalidValueException $e) {
                $exceptions->addException($e);
            }
        }

        if ($exceptions->hasExceptions()) {
            throw $exceptions;
        }

        return $values;
    }

    /**
     * Get a value
     * Priority goes as follows:
     *  post -> get
     * @param $key
     * @param bool|false $default_value
     * @param bool|false $throw_exception
     * @return mixed
     * @throws InvalidValueException
     */
    protected function getRequestValue($key, $default_value = false, $throw_exception = false)
    {
        return Tools::getRequestValue($key, $default_value, $throw_exception);
    }

    /**
     * Get a session value
     * @param $key
     * @param bool|false $default_value
     * @param bool|false $throw_exception
     * @return mixed
     * @throws InvalidValueException
     */
    protected function getSessionValue($key, $default_value = false, $throw_exception = false)
    {
        $session_values = $this->session->getValues();
        if (isset($session_values[$key])) {
            return $session_values[$key];
        }
        if ($throw_exception) {
            throw new InvalidValueException(sprintf("Value for '%s' could not be found.", $key));
        }
        return $default_value;
    }

    /**
     * @param $identifier
     * @return ControllerReference
     * @throws \core\exceptions\InvalidControllerException
     */
    protected function getReference($identifier)
    {
        return ControllerHandler::getInstance()->getReference($identifier);
    }

}