<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 8-3-2016
 * Time: 11:17
 */

namespace core\classes\controller;

/**
 * Class ControllerSession
 * @package core\classes\controller
 */
class ControllerSession
{

    /** @var ControllerReference */
    protected $reference;

    /** @var array */
    protected $values = array();

    /**
     * ControllerSession constructor.
     * @param ControllerReference $reference
     */
    public function __construct(ControllerReference $reference)
    {

        $this->reference = $reference;

        // Start the session if not already started
        if (!isset($_SESSION)) {
            session_start();
        }

    }

    /**
     * Load session for this controller
     * If the session is meant for this controller it will be initializes, otherwise it won't
     * @return bool
     */
    public function load()
    {

        // If there's no session
        // Don't initialize
        if (!isset($_SESSION['controller'])) {
            return false;
        }

        // If identifier is not set, or if the identifier is not the same as this reference's identifier
        // Don't initialize
        if (!isset($_SESSION['controller']['identifier'])
            || $this->reference->getIdentifier() !== $_SESSION['controller']['identifier'])
        {
            return false;
        }

        // Set the values if it was meant for this controller
        $this->values =  $_SESSION['controller']['values'];

        return true;
    }

    /**
     * Saves the values to the actual session
     * This will clear the previous values
     */
    public function save()
    {
        $this->clear();
        $_SESSION['controller'] = array(
            'identifier' => $this->reference->getIdentifier(),
            'values' => $this->values
        );
    }

    /**
     * Clear controller session from the actual session
     */
    public function clear()
    {
        unset($_SESSION['controller']);
    }

    /**
     * @param $key
     * @param $value
     */
    public function setValue($key, $value)
    {
        $this->values[$key] = $value;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getValue($key)
    {
        return $this->values[$key];
    }

    /**
     * @param $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }

    /**
     * @return array
     */
    public function &getValues()
    {
        return $this->values;
    }

}