<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 17-2-2016
 * Time: 09:28
 */

namespace core\classes\controller;

use core\exceptions\InvalidControllerException;

/**
 * Class ControllerHandler
 * @package core\classes\controller
 */
final class ControllerHandler
{

    /** @var ControllerHandler */
    private static $instance;

    /** @var ControllerDeclaration[] */
    private $declarations = array();

    /**
     * ControllerHandler constructor.
     */
    private function __construct() {}

    /**
     * Register a controller declaration
     * @param ControllerDeclaration $declaration
     */
    public function register(ControllerDeclaration $declaration)
    {
        $this->declarations[] = $declaration;
    }

    /**
     * Get a ControllerReference by its identifier
     * @param string $identifier
     * @return ControllerReference
     * @throws InvalidControllerException
     */
    public function getReference($identifier)
    {

        // Get the declaration by its identifier
        $declaration = $this->getDeclaration($identifier);

        // Throw error if declaration does not exist
        if ($declaration == false) {
            throw new InvalidControllerException(
                sprintf("Controller with identifier '%s' has not been declared or does not exist.", $identifier));
        }

        return new ControllerReference($declaration);
    }

    /**
     * Get a ControllerDeclaration by its identifier
     * @param string $identifier
     * @return ControllerDeclaration|false
     */
    private function getDeclaration($identifier)
    {
        foreach ($this->declarations as $declaration) {
            if ($declaration->getIdentifier() == $identifier) {
                return $declaration;
            }
        }
        return false;
    }

    /**
     * Generate content mod rewrite for .htaccess file
     * @return string
     */
    public function generateModRewrite()
    {
        // TODO: Maybe... (nice to have)
    }

    /**
     * @return ControllerHandler
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

}