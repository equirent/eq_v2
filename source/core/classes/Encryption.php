<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 3-3-2016
 * Time: 09:00
 */

namespace core\classes;


use core\exceptions\CoreException;
use core\exceptions\EncryptionException;
use core\lib\defuse\crypto\Crypto;
use core\lib\defuse\crypto\exceptions\CryptoException;
use core\lib\defuse\crypto\exceptions\CryptoTestFailedException;

class Encryption
{

    /** @var string */
    protected $key;

    /**
     * Encryption constructor.
     * @param string $key
     */
    public function __construct($key)
    {
        $this->key = $key;
    }

    /**
     * @param string $string
     * @return string
     * @throws CoreException
     * @throws EncryptionException
     */
    public function encrypt($string)
    {
        try {
            return Crypto::encrypt($string, $this->key);
        } catch (CryptoTestFailedException $e) {
            throw new CoreException('Crypto requires OpenSSL PHP extension to be enabled');
        } catch (CryptoException $e) {
            throw new EncryptionException('Failed to encrypt', $e->getMessage());
        }
    }

    /**
     * @param string $string
     * @return string
     * @throws CoreException
     * @throws EncryptionException
     */
    public function decrypt($string)
    {
        try {
            return Crypto::decrypt($string, $this->key);
        } catch (CryptoTestFailedException $e) {
            throw new CoreException('Crypto requires OpenSSL PHP extension to be enabled');
        } catch (CryptoException $e) {
            throw new EncryptionException('Failed to decrypt', $e->getMessage());
        }

    }

    /**
     * @return string
     * @throws CoreException
     * @throws EncryptionException
     */
    public function createNewRandomKey()
    {
        try {
            return Crypto::createNewRandomKey();
        } catch (CryptoTestFailedException $e) {
            throw new CoreException('Crypto requires OpenSSL PHP extension');
        } catch (CryptoException $e) {
            throw new EncryptionException('Failed to create random new key: '.$e->getMessage());
        }
    }

}