<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 17-2-2016
 * Time: 09:06
 */

namespace core\classes;

use core\exceptions\CoreException;
use core\exceptions\InvalidValueException;

/**
 * Class Tools
 * @package core\classes
 */
class Tools
{

    /**
     * Get a value
     * Priority goes as follows:
     *  post -> get
     * @param $key
     * @param bool|false $default_value
     * @param bool|false $throw_exception
     * @return mixed
     * @throws InvalidValueException
     */
    public static function getRequestValue($key, $default_value = false, $throw_exception = false)
    {
        if (isset($_POST[$key])) {
            return $_POST[$key];
        }
        if (isset($_GET[$key])) {
            return $_GET[$key];
        }
        if ($throw_exception) {
            throw new InvalidValueException(sprintf("Value for '%s' could not be found.", $key));
        }
        return $default_value;
    }

    /**
     * Get a post value
     * @param $key
     * @param bool|false $default_value
     * @param bool|false $throw_exception
     * @return mixed
     * @throws InvalidValueException
     */
    public static function getPostValue($key, $default_value = false, $throw_exception = false)
    {
        if (isset($_POST[$key])) {
            return $_POST[$key];
        }
        if ($throw_exception) {
            throw new InvalidValueException(sprintf("Value for '%s' could not be found.", $key));
        }
        return $default_value;
    }

    /**
     * Get a get value
     * @param $key
     * @param bool|false $default_value
     * @param bool|false $throw_exception
     * @return mixed
     * @throws InvalidValueException
     */
    public static function getGetValue($key, $default_value = false, $throw_exception = false)
    {
        if (isset($_GET[$key])) {
            return $_GET[$key];
        }
        if ($throw_exception) {
            throw new InvalidValueException(sprintf("Value for '%s' could not be found.", $key));
        }
        return $default_value;
    }

    /**
     * Format a path
     * @param string $path
     * @param bool|false $tailing_separator
     * @param string $separator
     * @return mixed
     */
    public static function formatPath($path, $tailing_separator = false, $separator = DIRECTORY_SEPARATOR)
    {
        return str_replace('/', $separator, rtrim(preg_replace('/[\\/\\\\]+/', '/', $path), '/'))
            .($tailing_separator ? $separator : '');
    }

    /**
     * Format an url
     * @param $url
     * @return string|false
     */
    public static function formatUrl($url)
    {
        if (preg_match('/^(https?:\\/\\/)?(.*)$/', $url, $matches)) {
            return $matches[1].preg_replace('/[\/\\\]+/', '/', $matches[2]);
        }
        return false;
    }

    /**
     * @param $url
     * @param $args
     * @return string
     */
    public static function appendQueryString($url, $args)
    {
        if (count($args)) {
            $arr = array();
            foreach ($args as $key => $value) {
                $arr[] = $key.'='.urlencode($value);
            }
            if (strstr($url, '?')) {
                return $url.'&'.implode('&', $arr);
            } else {
                return $url.'?'.implode('&', $arr);
            }
        }
        return $url;
    }

}