<?php

/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 6-12-2015
 * Time: 15:01
 */

namespace core\classes;

use core\exceptions\CoreException;

/**
 * Class Template
 * @package core\classes
 */
class Template
{

    /** @var string */
    protected $content;

    /** @var array */
    protected $vars = array();

    /** @var bool */
    protected $is_file = true;

    /**
     * Template constructor.
     * @param string $content
     * @param array $vars
     * @param bool|false $is_file Whether the content is a path to a file or code
     */
    public function __construct($content = null, $vars = array(), $is_file = true)
    {
        $this->content = $content;
        $this->vars = $vars;
        $this->is_file = $is_file;
    }

    /**
     * @param $vars
     */
    public function assign($vars)
    {
        $this->vars = array_merge($this->vars, $vars);
    }

    /**
     * Prints build template
     * @return TemplateObject
     * @throws TemplateException
     */
    public function show()
    {

        // Validate
        $this->validate();

        // Build & show template
        new TemplateObject($this->content, $this->vars, $this->is_file);

    }

    /**
     * Returns build template
     * @return string
     * @throws TemplateException
     */
    public function fetch()
    {

        // Validate
        $this->validate();

        // Start buffer
        ob_start();

        // Build template
        new TemplateObject($this->content, $this->vars, $this->is_file);

        // Return output of buffer
        return ob_get_clean();
    }

    /**
     * @throws TemplateException
     */
    protected function validate()
    {

        // Throw exception if empty
        if ($this->is_file && $this->content == '') {
            throw new TemplateException('Path to file can not be empty');
        }

        // Throw exception if the file does not exist
        if ($this->is_file && !file_exists($this->content)) {
            throw new TemplateException(sprintf("'%s' could not be found", $this->content));
        }

    }

}

/**
 * Class TemplateObject
 * @package core\classes
 */
class TemplateObject
{

    /**
     * TemplateObject constructor.
     * @param string $content
     * @param array $vars
     * @param bool $is_file
     */
    public function  __construct($content, $vars = array(), $is_file = true)
    {

        // Set as object's variables, so we can access them with $this
        foreach ($vars as $key => $var) {
            $this->{$key} = $var;
        }

        // Execute template
        if ($is_file) {
            include $content;
        } else {
            eval('?>'.$content);
        }

    }

}

/**
 * Class TemplateException
 * @package core\classes
 */
class TemplateException extends CoreException {}