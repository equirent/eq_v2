<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 7-12-2015
 * Time: 20:21
 */

namespace core\classes\config;

/**
 * Class ConfigCore
 * @package core\config
 */
abstract class ConfigCore
{

    /** @var array */
    private static $instances = array();

    /** @var array */
    private $values;

    /**
     * Config constructor.
     */
    protected function __construct()
    {
        $this->values = array();
    }

    /**
     * @param $name
     * @param $value
     */
    public static function set($name, $value)
    {
        static::getInstance()->values[$name] = $value;
    }

    /**
     * @param $name
     * @return mixed
     */
    protected static function get($name)
    {
        return static::getInstance()->values[$name];
    }

    /**
     * @return ConfigCore
     */
    private static function getInstance()
    {
        if (!isset(self::$instances[static::class])) {
            self::$instances[static::class] = new static();
        }
        return self::$instances[static::class];
    }

}