<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 7-12-2015
 * Time: 20:20
 */

namespace core\classes\config;

/**
 * Class Settings
 * @package core\config
 */
class Settings extends ConfigCore
{

    /**
     * @return bool
     */
    public static function isDevelopMode()
    {
        return (bool)self::get('DEVELOP_MODE');
    }

    /**
     * @return string
     */
    public static function isSEOUrls()
    {
        return (bool)self::get('SEO_URLS');
    }

    /**
     * @return array
     */
    public static function getDatabaseConnectionInfo()
    {
        return array(
            'host' => self::get('DATABASE_HOST'),
            'user' => self::get('DATABASE_USER'),
            'password' => self::get('DATABASE_PASS'),
            'name' => self::get('DATABASE_NAME')
        );
    }

    /**
     * @return string
     */
    public static function getLandingPageIdentifier()
    {
        return self::get('LANDING_PAGE_IDENTIFIER');
    }

    /**
     * @return string
     */
    public static function getEncryptionKey()
    {
        return self::get('ENCRYPTION_KEY');
    }

}