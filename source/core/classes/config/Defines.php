<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 7-12-2015
 * Time: 20:01
 */

namespace core\classes\config;

use core\classes\Tools;

/**
 * Class Defines
 * @package core\config
 */
class Defines extends ConfigCore
{

    /**
     * @return string
     */
    public static function getVersion()
    {
        return self::get('VERSION');
    }

    /**
     * @return mixed
     */
    public static function getVersionNR()
    {
        return self::get('VERSION_NR');
    }

    /**
     * Returns the root directory url
     * @return string
     */
    public static function getRootDirUrl()
    {
        return self::get('ROOT_DIR_URL');
    }

    /**
     * Returns the root initialize file url
     * @return string
     */
    public static function getRootFileUrl()
    {
        return self::get('ROOT_FILE_URL');
    }

    /**
     * @return string
     */
    public static function getRootDir()
    {
        return self::get('ROOT_DIR');
    }

}