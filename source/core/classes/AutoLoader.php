<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 12-1-2016
 * Time: 12:08
 */

namespace core\classes;
use core\exceptions\AutoLoadException;

/**
 * Class AutoLoader
 * @package core\classes
 */
class AutoLoader
{

    /** @var string */
    protected $root;

    /**
     * AutoLoader constructor.
     * @param string $root
     */
    function __construct($root)
    {
        $this->root = rtrim($root, '/\\');
        spl_autoload_register(array($this, 'load'));
    }

    /**
     * Partially based on http://www.php-fig.org/psr/psr-0/
     * @param string $class
     * @throws AutoLoadException
     */
    protected function load($class)
    {

        $file = $this->getFileFromClass($class);

        // Throw exception if class could not be found
        if (!file_exists($file)) {
            throw new AutoLoadException(sprintf("Class '%s' could not be found in file '%s'", $class, $file));
        }

        // Include the class
        require $file;

        // Magic method class constructor (quote: 'quick, simple, effective... and ugly.')
        if (\method_exists($file, '__init__')) {
            \call_user_func(array($file, '__init__'));
        }

    }

    /**
     * @param string $class
     * @return string
     */
    protected function getFileFromClass($class)
    {

        $class = ltrim($class, '\\');

        $file = '';

        // Check if there's a namespace
        if ($last_namespace_pos = strrpos($class, '\\')) {

            // Get the class name from the whole bunch
            $class_name = substr($class, $last_namespace_pos + 1);

            // Concatenate the namespace to the file
            $namespace = strtolower(substr($class, 0, $last_namespace_pos)); // Directories must always be lower cased
            $file .= str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;

        } else {
            $class_name = $class;
        }

        // The whole file path relative to root
        // _ represents a directory in a class's name
        $file = $this->root.DIRECTORY_SEPARATOR.$file.str_replace('_', DIRECTORY_SEPARATOR, $class_name) . '.php';

        return $file;
    }

}