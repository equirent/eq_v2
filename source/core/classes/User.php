<?php

namespace core\classes;

use core\classes\config\Settings;
use core\classes\Encryption;
use core\database\Database;
use core\database\Query;
use core\exceptions\EncryptionException;
use core\lib\kurtzalead\dbconnect\exceptions\connection\ExecuteException;

/**
 * Class User
 * @package core\classes
 */
class User
{

    /** @var int */
    public $id;

    /** @var string */
    public $email;

    /** @var string */
    public $first_name;

    /** @var string */
    public $last_name;

    /** @var string */
    protected $auth_token;

    /** @var bool */
    protected $loaded;

    /**
     * User constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
        $this->load();
    }

    /**
     * Whether the User object is loaded or not
     * @return bool
     */
    public function isLoaded()
    {
        return $this->loaded;
    }

    /**
     * Loads the user
     * @return bool Whether it succeeded to load the user
     */
    public function load()
    {

        // Select user info query
        $query = new Query(
            'SELECT
                `email`,
                `first_name`,
                `last_name`,
                `auth_token`
            FROM
                `user`
            WHERE
                `user_id` = ?;',
            array($this->id)
        );
        $result = Database::getInstance()->executeR($query);

        // Return false if no result
        if (!isset($result[0])) {
            $this->loaded = false;
            return false;
        }

        // Set the properties of the user
        $this->email = $result[0]['email'];
        $this->first_name = $result[0]['first_name'];
        $this->last_name = $result[0]['last_name'];
        $this->auth_token = $result[0]['auth_token'];

        $this->loaded = true;
        return true;
    }

    /**
     * Update the user in the database
     */
    public function update()
    {
        $query = new Query(
            'UPDATE
                `user`
            SET
                `first_name` = ?,
                `last_name` = ?
            WHERE
                `user_id` = ?;',
            array($this->first_name, $this->last_name, $this->id)
        );
        Database::getInstance()->execute($query);
    }

    /**
     * Remembers the user by creating a token that is stored in the database and a cookie
     * @param bool $browser_session Whether we want to remove the session when the browser closes
     * @return bool
     */
    public function remember($browser_session = true)
    {

        // Create a new random token
        $encryption = new Encryption(Settings::getEncryptionKey());
        $auth_token = md5($this->id.$encryption->createNewRandomKey());

        // JSON encode and encrypt the auth token
        try {
            $token = base64_encode($encryption->encrypt(json_encode(array(
                'id' => $this->id,
                'token' => $auth_token
            ))));
        } catch (EncryptionException $e) {
            return false;
        }

        // Set the cookie with the token
        setcookie(md5('auth_token'), $token, $browser_session ? null : time() + 604800); // Remember for 4 weeks

        // Save token to the database
        $query = new Query(
            'UPDATE
                `user`
            SET
                `auth_token` = ?
            WHERE
                `user_id` = ?;',
            array($auth_token, $this->id)
        );
        Database::getInstance()->execute($query);

        return true;
    }

    /**
     * Logout the user
     */
    public static function logout()
    {

        // Simply delete the cookie
        setcookie(md5('auth_token'), 0, time() - 3600);

    }

    /**
     * Get user by an encrypted authentication token
     * @param string $encrypted_auth_token
     * @return User|false False if failed
     */
    public static function getByToken($encrypted_auth_token)
    {

        $encryption = new Encryption(Settings::getEncryptionKey());

        // Decrypt token to array
        try {
            $array = json_decode($encryption->decrypt(base64_decode($encrypted_auth_token)), true);
        } catch (EncryptionException $e) {
            return false;
        }

        // Return false if it's not valid
        if (!isset($array['id']) || !isset($array['token'])) {
            return false;
        }

        // Create new user
        $user = new self((int)$array['id']);

        // Return false if user was not loaded
        if (!$user->isLoaded()) {
            return false;
        }

        // Compare the token from the cookie and database
        if ($user->auth_token == null || $user->auth_token !== $array['token']) {
            return false;
        }

        // Return the user
        return $user;
    }

    /**
     * Get the user that was remembered by the browser
     * @return User|false False if failed
     */
    public static function getRemembered()
    {

        // Make sure cookie is set
        if (!isset($_COOKIE[md5('auth_token')])) {
            return false;
        }

        $user = self::getByToken($_COOKIE[md5('auth_token')]);

        // If no user returned destroy the cookie
        if ($user == false) {
            setcookie(md5('auth_token'), 0, time() - 3600);
            return false;
        }

        return $user;
    }

    /**
     * Get the user by email address and password
     * @param string $email
     * @param string $password
     * @return User|false
     */
    public static function getByEmailAndPassword($email, $password)
    {

        // Select the password
        $query = new Query(
            'SELECT
                `user_id`, `password`
            FROM
                `user`
            WHERE
                `email` = ?;',
            array($email)
        );
        $result = Database::getInstance()->executeR($query);

        // Verify the password
        if (!isset($result[0]['password']) || password_verify($password, $result[0]['password']) == false) {
            return false;
        }

        // Create new user
        $user = new self((int)$result[0]['user_id']);

        // Return false if user was not loaded
        if (!$user->isLoaded()) {
            return false;
        }

        // Return the user
        return $user;
    }

    /**
     * Register a new user
     * @param string $email
     * @param string $password
     * @return User|false
     */
    public static function register($email, $password)
    {

        // Validate the email address
        if (!\filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        // Hash the password
        $password = \password_hash($password, PASSWORD_BCRYPT);

        // Insert the email and password
        $query = new Query(
            'INSERT INTO
                `user` (
                    `email`,
                    `password`
                )
            VALUES (
                ?, ?
            );',
            array($email, $password)
        );

        // Return false if the execution fails
        try {
            Database::getInstance()->execute($query);
        } catch (ExecuteException $e) {
            return false;
        }

        // Get the id
        $query = new Query(
            'SELECT
                `user_id`
            FROM
                `user`
            WHERE
              `email` = ?;',
            array($email)
        );
        $result = Database::getInstance()->executeR($query);

        // Return false if we didn't get an id
        if (!isset($result[0]['user_id'])) {
            return false;
        }

        // Create user
        $user = new User($result[0]['user_id']);

        // Return the user
        return $user;
    }

}