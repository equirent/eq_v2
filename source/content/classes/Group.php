<?php
/**
 * Created by PhpStorm.
 * User: Jos R.
 * Date: 2-3-2016
 * Time: 10:03
 */

namespace core\classes;


class Group
{

    public $id;
    public $organisationId;
    public $groupSize;
    public $persons;

    public function addPerson()
    {
        return $this->persons;
    }

    public function getPersons()
    {
        return $this->persons;
    }

    public function deletePerson()
    {
        return $this;
    }

}