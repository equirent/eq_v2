<?php
/**
 * Created by PhpStorm.
 * User: Gebruiker
 * Date: 17-3-2016
 * Time: 09:22
 */

namespace content\classes;
use core\exceptions\InvalidValueException;

/**
 * Class Color
 * @package content\classes
 */
class Color
{

    /** @var int */
    public $red;

    /** @var int */
    public $green;

    /** @var int */
    public $blue;

    /**
     * Color constructor.
     * @param int $red
     * @param int $green
     * @param int $blue
     * @throws InvalidValueException
     */
    public function __construct($red = 255, $green = 255, $blue = 255)
    {

        // The size of a color must be the size of a unsigned Byte
        if ($red > 255 || $red < 0) {
            throw new InvalidValueException("Expected value of red to be in between 0 and 255");
        }
        if ($green > 255 || $green < 0) {
            throw new InvalidValueException("Expected value of green to be in between 0 and 255");
        }
        if ($blue > 255 || $blue < 0) {
            throw new InvalidValueException("Expected value of blue to be in between 0 and 255");
        }

        $this->$red = $red;
        $this->$green = $green;
        $this->$blue = $blue;

    }

    /**
     * @return string
     */
    public function toString()
    {
        return implode(";", array((int)$this->red, (int)$this->green, (int)$this->blue));
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array(
            "r" => (int)$this->red,
            "g" => (int)$this->green,
            "b" => (int)$this->blue
        );
    }

    /**
     * @param $string
     * @return Color|false False if invalid
     */
    public static function stringToColor($string)
    {

        $rgb = explode(";", $string);

        if (count($rgb) !== 3) {
            return new self(255, 255, 255);
        }

        return new self((int)$rgb[0], (int)$rgb[1], (int)$rgb[2]);
    }



}