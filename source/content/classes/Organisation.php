<?php
/**
 * Created by PhpStorm.
 * User: Jos R.
 * Date: 2-3-2016
 * Time: 10:04
 */

namespace content\classes;


use core\database\Database;
use core\database\Query;
use core\lib\kurtzalead\dbconnect\DatabaseConnection;

class Organisation
{

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $location;

    /** @var Color */
    public $color;

    /** @var string */
    public $abbreviation;

    /**
     * @param int $start
     * @param int $limit
     * @return Organisation[]
     */
    public static function getAll($start = 0, $limit = 20)
    {

        $query = new Query("
            SELECT
              `organisation_id`,
              `name`,
              `location`,
              `color`,
              `abbreviation`
            FROM
              `organisation`
            WHERE
              `boolean_deleted` = 0
            LIMIT
              ?, ?;
        ", array($start, $limit));
        $result = Database::getInstance()->executeR($query);

        $organisations = array();

        foreach ($result as $record) {

            $organisation = new Organisation();

            $organisation->id = $record["organisation_id"];
            $organisation->name = $record["name"];
            $organisation->location = $record["location"];
            $organisation->color = Color::stringToColor($record["color"]);
            $organisation->abbreviation = $record["abbreviation"];

            $organisations[] = $organisation;

        }

        return $organisations;
    }

    /**
     * @return bool
     */
    public function insert()
    {

        $query = new Query("
            INSERT INTO
              organisation(
                `name`,
                `location`,
                `color`,
                `abbreviation`
              )
            VALUES(?, ?, ?, ?);
        ", array($this->name, $this->location, $this->color->toString(), $this->abbreviation));
        Database::getInstance()->execute($query);

        $query = new Query("
            SELECT LAST_INSERT_ID();
        ");
        $result = Database::getInstance()->executeR($query, false, DatabaseConnection::R_NUM);

        if (!isset($result[0][0])) {
            return false;
        }

        $this->id = $result[0][0];

        return true;
    }

    public function delete()
    {

        $query = new Query("
            UPDATE
              `organisation`
            SET
              `boolean_deleted` = 1
            WHERE
              `organisation_id` = ?;
        ", array($this->id));
        Database::getInstance()->execute($query);

    }

    public function update()
    {

        $query = new Query("
            UPDATE
              `organisation`
            SET
              `name` = ?,
              `location` = ?,
              `color` = ?,
              `abbreviation` = ?
            WHERE
              `organisation_id` = ?
        ", array($this->name, $this->location, $this->color->toString(), $this->abbreviation, $this->id));
        Database::getInstance()->execute($query);
    }
}