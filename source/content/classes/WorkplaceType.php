<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 8-3-2016
 * Time: 08:53
 */

namespace content\classes;


use core\database\Database;
use core\database\Query;

class WorkplaceType
{

    /** @var  int */
    protected $id;

    /** @var  string */
    protected $name;

    /** @var  string */
    protected $description;

    /** @var  Workplace[] */
    protected $workplaces;

    /** @var  bool */
    protected $loaded;

    /**
     * WorkplaceType constructor.
     * @param int|null $id
     */
    public function __construct($id = null)
    {
        if($id){
            $this->id = $id;
            $this->load();
        }
    }

    /**
     * @return bool
     */
    public function load(){

        $query = new Query('
            SELECT
                `name`,
                `desctiption`
            FROM
                `workplace_type`
            WHERE
                `workplace_type_id` = ?;
                ',
            array($this->id)
        );
        $result = Database::getInstance()->executeR($query);

        if (!isset($result[0])) {
            $this->loaded = false;
            return false;
        }

        $this->name = $result[0]['name'];
        $this->description = $result[0]['description'];

        $this->loaded = true;
        return true;
    }

    public function insert(){
        $query = new Query(
            'INSERT INTO
                `workplace_type`(
                `name`,
                `desctiption`)
              VALUES
                (?,?)
                ',
            array($this->name,$this->description)
        );
        $result = Database::getInstance()->executeR($query);
    }

    public function update(){
$query = new Query (


);

    }

    public function save(){
        if ($this->id){
            $this->update();
        } else {
            $this->insert();
        }
    }

    public function isLoaded(){
        return $this->loaded;
    }
}