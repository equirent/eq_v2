<?php
/**
 * Created by PhpStorm.
 * User: Jos R.
 * Date: 2-3-2016
 * Time: 10:03
 */

namespace core\classes;


class Calender
{
    public $events;

    public function addEvent()
    {
        return $this->events;
    }

    public function deleteEvent()
    {
        return $this;
    }

    public function getEvents()
    {
        return $this->events;
    }
}