<?php
/**
 * Created by PhpStorm.
 * User: Jos R.
 * Date: 2-3-2016
 * Time: 10:03
 */

namespace core\classes;


class Event
{
    public $id;
    public $machines;
    public $startDate;
    public $endDate;
    public $description;
    public $color;
    public $groups;

    public function getGroups()
    {
        return $this->groups;
    }

    public function getMachines()
    {
    return $this->machines;
    }

    public function addGroup($groups)
    {
        $this->groups = $groups;
    }

    public function addMachine($machine)
    {
        $this->machines = $machine;
    }

    public function deleteGroup()
    {
        return $this;
    }

    public function deleteMachine()
    {
        return $this;
    }

}