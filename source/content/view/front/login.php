<form method="POST" action="<?php echo $this->login_url; ?>" id="form">
    <fieldset>
        <div class="legend-div">
            <legend><i class="fa fa-globe fa-2x"></i><p id="legend-text"> Login Form</p></legend>
        </div>
        <?php if (!empty($this->error_message)) : ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $this->error_message; ?>
        </div>
        <?php endif; ?>
        <div class="input-group">
            <span class="input-group-addon" id="username-icon"><i class="fa fa-user"></i></span>
            <input name="name" type="text" class="form-control" placeholder="Username" aria-describedby="username-icon">
        </div>
        <div class="input-group" id="input-password">
            <span class="input-group-addon" id="password-icon"><i class="fa fa-lock lock-icon"></i></span>
            <input name="password" type="password" class="form-control" placeholder="Password" aria-describedby="password-icon">
        </div>
        <div id="remember">
            <input type="checkbox" name="remember" id="login-remember">
            <label for="login-remember">Remember me</label>
        </div>
        <button class="btn btn-default" id="login-btn" type="submit">Login</button>
    </fieldset>
</form>