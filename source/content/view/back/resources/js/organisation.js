app.controller('organisationController', function ($scope, $http) {

    // table title
    $scope.tableTitle = 'Organisaties';

    $scope.selected = {};
    $scope.selected.updating = {};

    $scope.select = function () {
        $scope.selected.item = this;
        $scope.selected.updating.name = this.value.name;
        $scope.selected.updating.location = this.value.location;
        $scope.selected.updating.color = this.value.color;
        $scope.selected.updating.abbreviation = this.value.abbreviation;

    };

    // get table content.
    $scope.updateList = function () {
        $http.post("?controller=api-organisation", {
            "method": "getAll"
        }).success(function (data) {
            $scope.data = data.data;
        });
    };
    $scope.updateList();

    // insert organisations.
    $scope.insert = function () {
        $http.post("?controller=api-organisation", {
            "method": "insert",
            "org_name" : $scope.name,
            "org_location" : $scope.location,
            "org_color" : "255;255;255",
            "org_abbreviation" : $scope.abbreviation
        }).success(function () {
            $scope.updateList();
        });
    };

    //delete an organisation.
    $scope.delete = function () {
        $http.post("?controller=api-organisation", {
            "method" : "delete",
            "org_id" : this.value.id
        }).success(function () {
            $scope.updateList();
        });
    };

    $scope.update = function () {
        $http.post("?controller=api-organisation", {
            "method": "update",
            "org_id" : $scope.selected.item.value.id,
            "org_name" : $scope.selected.updating.name,
            "org_location" : $scope.selected.updating.location,
            "org_color" : $scope.selected.updating.color,
            "org_abbreviation" : $scope.selected.updating.abbreviation
        }).success(function () {
            $scope.updateList();
        });
    };

});