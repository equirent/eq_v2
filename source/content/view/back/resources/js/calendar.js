var app = angular.module("eQuiRent", [
    'mwl.calendar', 'ui.bootstrap', 'ngAnimate'// Depancies for the calendar
]);

app.service('alert', function(){});
// Calendar
app.controller('calendar', function(moment, alert, $http, $window) {

    var vm = this;

    //These variables MUST be set as a minimum for the calendar to work
    vm.calendarView = 'month';
    vm.viewDate = new Date();
    vm.events = [

    ];

    vm.isCellOpen = true;

    vm.eventClicked = function(event) {
        alert.show('Clicked', event);
    };

    vm.eventEdited = function(event) {
        alert.show('Edited', event);
    };

    vm.eventDeleted = function(event) {
        alert.show('Deleted', event);
    };

    vm.eventTimesChanged = function(event) {
        alert.show('Dropped or resized', event);
    };

    vm.toggle = function($event, field, event) {
        $event.preventDefault();
        $event.stopPropagation();
        event[field] = !event[field];
    };

    vm.insertData = function(){
        $http.post("/../content/view/back/event_db/insert_event.php", {
            'start': vm.start,
            'end': vm.end
        }).success(function(){
            console.log("data inserted successfully");
        })
    }

});
