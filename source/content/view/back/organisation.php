<div ng-controller="organisationController">
    <div class="row">
        <div class="col-lg-2 pull-right col-lg-pull-1">
<!--            <div class="addEventBlock sidebar-left-border-bottom">-->
                <button type="button" class="btn btn-primary pull-right bottom-margin"  data-toggle="modal" data-target="#modal-add">Voeg organisatie toe</button>
<!--            </div>-->
        </div>
    </div>

    <div class="row">
        <div class="col-lg-10 pull-right col-lg-pull-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ tableTitle }}</div>
                <table class="table"> <thead> <tr>
                        <th>#</th>
                        <th>Afkorting</th>
                        <th>Naam</th>
                        <th>Locatie</th>
                        </tr>
                    </thead>
                    <tbody ng-repeat="value in data track by $index">
                        <tr>
                            <th scope="row">{{ $index + 1 }}</th>
                            <td>{{ value.abbreviation }}</td>
                            <td>{{ value.name }}</td>
                            <td>{{ value.location }}</td>
                            <td><a type="button" class="fa fa-1x fa-trash" ng-click="delete()"></a></td>
                            <td><a type="button" class="fa fa-1x fa-pencil" ng-click="select()" data-toggle="modal" data-target="#modal-edit"></a></td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

<!--Add modal-->
    <div id="modal-add" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Voeg organisatie toe</h4>
                </div>
                <div class="modal-body">
                    <div class="start-end-date-modal">
                        <form method="post">
                            <fieldset>
                                <div class="input-group">
                                    <span>Naam: </span>
                                    <input type="text" class="form-control" ng-model="name">
                                </div>
                                <div class="input-group">
                                    <span>Locatie: </span>
                                    <input type="text" class="form-control" ng-model="location">
                                </div>
                                <div class="input-group">
                                    <span>Afkorting: </span>
                                    <input type="text" class="form-control" ng-model="abbreviation">
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary" ng-click="insert()" data-dismiss="modal">Opslaan</button>
                </div>
            </div>
        </div>
    </div>
<!--Edit modal-->
    <div id="modal-edit" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Voeg organisatie toe</h4>
                </div>
                <div class="modal-body">
                    <div class="start-end-date-modal">
                        <form method="post">
                            <fieldset>
                                <div class="input-group">
                                    <span>Naam: </span>
                                    <input type="text" class="form-control" ng-model="selected.updating.name">
                                </div>
                                <div class="input-group">
                                    <span>Locatie: </span>
                                    <input type="text" class="form-control" ng-model="selected.updating.location">
                                </div>
                                <div class="input-group">
                                    <span>Afkorting: </span>
                                    <input type="text" class="form-control" ng-model="selected.updating.abbreviation">
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                    <button type="button" class="btn btn-primary" ng-click="update()" data-dismiss="modal">Opslaan</button>
                </div>
            </div>
        </div>
    </div>
</div>
