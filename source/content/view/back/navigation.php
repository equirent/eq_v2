<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-axpanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo $this->home_url; ?>">Equirent</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php foreach ($this->nav_urls as $url) : ?>
                <li><a href="<?php echo $url['url']; ?>"><?php echo $url['name']; ?></a></li>
                <?php endforeach; ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Welkom, <?php echo $this->user_name; ?></a></li>
                <li><a href="<?php echo $this->logout_url; ?>">Afmelden</a></li>
            </ul>
        </div>
    </div>
</nav>