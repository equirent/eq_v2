<style>
        .highlight {
            background:#00cccc;
        }

        .noselect {
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
        }
    </style>
<legend style="font-size: 50px; padding-left: 20px;">Workplace</legend>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4">
                <div class="btn-group">
                    <button class="btn btn-primary">
                        Workplace Types
                    </button>
                    <button data-toggle="dropdown" style="margin-right: 5px;" class="btn btn-primary pull-left dropdown-toggle">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#">MachineSoort1</a></li>
                        <li><a href="#">MachineSoort2</a></li>
                        <li><a href="#">MachineSoort3</a></li>
                        <li><a href="#">MachineSoort4</a></li>
                        <li><a href="#">MachineSoort5</a></li>
                    </ul>
                </div>

                <div class="btn-group">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#workplaceAdd">
                        Add Workplace
                    </button>
                </div>
            </div>

            <div class="button-group pull-right hide buttons " id="unhide">
                <!-- Button Edit -->
                <button class="btn btn-sm btn-warning">
                    <span class="glyphicon glyphicon-wrench"></span> Edit
                </button>
                <!-- Button Remove -->
                <button class="btn btn-sm btn-danger"t>
                    <span class="glyphicon glyphicon-trash"></span> Remove
                </button>
            </div>

        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="workplaceAdd" tabindex="-1" role="dialog" aria-labelledby="workplaceAddLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="createanimalLabel">Add Machine</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <form class="form-horizontal"action="./"method="post">
                            <input hidden="hidden" value="1" name="ajax">
                            <input hidden="hidden" value="add" name="ajax-method">
                            <fieldset>
                                <label class="col-md-4 control-label" for="workplaceSelect">Select Workplace</label>
                                <div class="col-md-5">
                                    <div class="dropdown">
                                        <button style="margin-bottom: 15px; margin-left: -5px;" required="false" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Workplace Select
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li role="presentation"><a role="menuitem">Machine Workplace #1</a></li>
                                            <li role="presentation"><a role="menuitem">Machine Workplace #2</a></li>
                                            <li role="presentation"><a role="menuitem">Machine Workplace #3</a></li>
                                            <li role="presentation"><a role="menuitem">Machine Workplace #4</a></li>
                                            <li role="presentation"><a role="menuitem">Machine Workplace #5</a></li>
                                            <li role="presentation"><a role="menuitem">Machine Workplace #6</a></li>
                                            <li role="presentation"><a role="menuitem">Machine Workplace #7</a></li>
                                            <li role="presentation" class="divider"></li>
                                            <li role="presentation"><a role="menuitem"><input id="addWorkplaceInput" name="addWorkplaceInput" type="text" placeholder="Add Workplace" class="form-control input-md"></li></a>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="machineName">Machine Name</label>
                                    <div class="col-md-5">
                                        <input id="machineName" name="machineName" type="text" placeholder="drill 1" class="form-control input-md" required="false">
                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="machineDescription">Machine Description</label>
                                    <div class="col-md-5">
                                        <input id="machineDescription" name="machineDescription" type="text" placeholder="description" class="form-control input-md" required="">

                                    </div>
                                </div>

                                <!-- Button (Double) -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="backBtn"></label>
                                    <div class="col-md-5">
                                        <button id="addBtn" name="addBtn" class="btn btn-success pull-right">Add Machine</button>
                                        <button id="backBtn" name="backBtn" data-dismiss="modal" class="btn btn-danger">Back</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <!----------------------------------------- Form End ------------------------------------------------>

                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <table class="table noselect">
        <thead>
        <tr>
            <th>Type</th>
            <th>Name</th>
            <th>Description</th>
            <th>Date Added</th>
            <th>Date Modified</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Machine Workplace</td>            <!-- soort van de machine -->
            <td>Machine Name</td>                       <!-- naam van de machine -->
            <td>Machine Description</td>    <!-- omschrijving wat de machine doet -->
            <td>01 March 2016</td>          <!-- waneer de machine is aangemaakt -->
            <td>24 March 2016</td>          <!-- waneer de machine voor het laatst bewerkt is -->
        </tr>
        <tr>
            <td>Machine Workplace</td>            <!-- soort van de machine -->
            <td>Machine Name</td>                       <!-- naam van de machine -->
            <td>Machine Description</td>    <!-- omschrijving wat de machine doet -->
            <td>01 March 2016</td>          <!-- waneer de machine is aangemaakt -->
            <td>24 March 2016</td>          <!-- waneer de machine voor het laatst bewerkt is -->
        </tr>
        <tr>
            <td>Machine Workplace</td>            <!-- soort van de machine -->
            <td>Machine Name</td>
            <td>Machine Description</td>    <!-- omschrijving wat de machine doet -->
            <td>01 March 2016</td>          <!-- waneer de machine is aangemaakt -->
            <td>24 March 2016</td>          <!-- waneer de machine voor het laatst bewerkt is -->
        </tr>
        <tr>
            <td>Machine Workplace</td>            <!-- soort van de machine -->
            <td>Machine Name</td>
            <td>Machine Description</td>    <!-- omschrijving wat de machine doet -->
            <td>01 March 2016</td>          <!-- waneer de machine is aangemaakt -->
            <td>24 March 2016</td>          <!-- waneer de machine voor het laatst bewerkt is -->
        </tr>
        <tr>
            <td>Machine Workplace</td>            <!-- soort van de machine -->
            <td>Machine Name</td>
            <td>Machine Description</td>    <!-- omschrijving wat de machine doet -->
            <td>01 March 2016</td>          <!-- waneer de machine is aangemaakt -->
            <td>24 March 2016</td>          <!-- waneer de machine voor het laatst bewerkt is -->
        </tr>
        </tbody>
    </table>
</div>
</div>
</div>
<script>
    $(document).ready(function () {
        $('tr').on('click', function () {
            console.log("Selected!");

            $(this).toggleClass('highlight').siblings().removeClass('highlight');
            $('.buttons').addClass('hide');

            if ($('tr').hasClass('highlight')) {
                $('.buttons').removeClass('hide');
            }
        });
    });
</script>