<?php
$connect = mysqli_connect("equirent.dev", "root", "", "equirent");
$data = json_decode(file_get_contents("http://equirent.dev/content/view/resources/organisations.js"));
$id = mysqli_real_escape_string($connect, $data->organisation_id);

$query = mysqli_query($connect, "
  UPDATE
    `organisation`
  SET
    `date_modified` = NOW(),
    `boolean_deleted` = 1
  WHERE
    `organisation_id` = ".$id."
  ") or die(mysqli_error($connect));

$data = array();

while($row = mysqli_fetch_array($query)){
    $data[] = $row;
}
print json_encode($data);