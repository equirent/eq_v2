<div ng-controller="calendar as vm">
<h2 class="text-center">{{ vm.calendarTitle }}</h2>

<div class="row">
    <div class="col-md-4 text-center">
        <!--Add event button-->
        <div class="">
            <button class="btn btn-primary pull-left" type="button" data-toggle="modal" data-target=".bs-modal-lg">
                Add Event
            </button>
        </div>
        <!-- Previous today and next day buttons-->
        <div class="btn-group text">
            <button
                class="btn btn-primary"
                mwl-date-modifier
                date="vm.viewDate"
                decrement="vm.calendarView">
                Previous
            </button>
            <button
                class="btn btn-default"
                mwl-date-modifier
                date="vm.viewDate"
                set-to-today>
                Today
            </button>
            <button
                class="btn btn-primary"
                mwl-date-modifier
                date="vm.viewDate"
                increment="vm.calendarView">
                Next
            </button>
        </div>
    </div>
    <div class="col-md-6 text-center">
        <!--year month week and day buttons-->
        <div class="btn-group">
            <label class="btn btn-primary" ng-model="vm.calendarView" uib-btn-radio="'year'">Year</label>
            <label class="btn btn-primary" ng-model="vm.calendarView" uib-btn-radio="'month'">Month</label>
            <label class="btn btn-primary" ng-model="vm.calendarView" uib-btn-radio="'week'">Week</label>
            <label class="btn btn-primary" ng-model="vm.calendarView" uib-btn-radio="'day'">Day</label>
        </div>
    </div>

</div>

<br>

<mwl-calendar
    events="vm.events"
    view="vm.calendarView"
    view-title="vm.calendarTitle"
    view-date="vm.viewDate"
    on-event-click="vm.eventClicked(calendarEvent)"
    on-event-times-changed="vm.eventTimesChanged(calendarEvent); calendarEvent.startsAt = calendarNewEventStart; calendarEvent.endsAt = calendarNewEventEnd"
    edit-event-html="'<i class=\'glyphicon glyphicon-pencil\'></i>'"
    delete-event-html="'<i class=\'glyphicon glyphicon-remove\'></i>'"
    on-edit-event-click="vm.eventEdited(calendarEvent)"
    on-delete-event-click="vm.eventDeleted(calendarEvent)"
    cell-is-open="vm.isCellOpen"
    day-view-start="08:00"
    day-view-end="17:00"
    day-view-split="15"
    cell-modifier="vm.modifyCell(calendarCell)">
</mwl-calendar>

<!--  Edit events  Modal-->
<div class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" style="color:red;" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Add Event</h4>
            </div>
            <!--Modal body-->
            <div class="modal-body" >
                <div class="start-end-date-group">
                    <form class="form-horizontal"action="./"method="post"">
                        <fieldset>
                            <label class="col-md-4 control-label" for="eventTitle">Title</label>
                            <div class="col-md-5">
                                <select id="hond" ng-model="event.machine" class="form-control">
                                    <option value="Machine 1">Machine 1</option>
                                    <option value="Machine 3"> Machine 2</option>
                                    <option value="Boormachienke">Boormachienke</option>
                                </select>
                            </div>
                            <br><br>
                            <label class="col-md-4 control-label" for="eventType">Type</label>
                            <div class="col-md-5">
                                <select ng-model="event.organisations" class="form-control">
                                    <option value="Organisatie 1">Organisatie</option>
                                    <option value="Organisatie 2">Warning</option>
                                    <option value="Organisatie 3">Info</option>
                                    <option value="Organisatie 4">Inverse</option>
                                    <option value="Organisatie 45">Success</option>
                                    <option value="Organisatie 6">Special</option>
                                </select>
                            </div>
                            <br><br>
                            <label class="col-md-4 control-label" for="startsAt">Starts at</label>
                            <div class="col-md-5">
                                <p class="input-group" style="max-width: 250px">
                                    <input
                                        type="text"
                                        class="form-control"
                                        readonly
                                        uib-datepicker-popup="dd MMMM yyyy"
                                        ng-model="event.start"
                                        is-open="event.startOpen"
                                        close-text="Close" >
                                            <span class="input-group-btn">
                                              <button
                                                  type="button"
                                                  class="btn btn-default"
                                                  ng-click="vm.toggle($event, 'startOpen', event)">
                                                  <i class="fa fa-calendar"></i>
                                              </button>
                                            </span>
                                </p>
                                <uib-timepicker
                                    ng-model="event.start"
                                    hour-step="1"
                                    minute-step="15"
                                    show-meridian="true">
                                </uib-timepicker>
                            </div>
                            <br><br>
                                <label class="col-md-4 control-label" for="endsAt">Ends at</label>
                                <div class="col-md-5">
                                <p class="input-group" style="max-width: 250px">
                                    <input
                                        type="text"
                                        class="form-control"
                                        readonly
                                        uib-datepicker-popup="dd MMMM yyyy"
                                        ng-model="event.end"
                                        is-open="event.endOpen"
                                        close-text="Close">
                                            <span class="input-group-btn">
                                              <button
                                                  type="button"
                                                  class="btn btn-default"
                                                  ng-click="vm.toggle($event, 'endOpen', event)">
                                                  <i class="fa fa-calendar"></i>
                                              </button>
                                            </span>
                                </p>
                                <uib-timepicker
                                    ng-model="event.end"
                                    hour-step="1"
                                    minute-step="15"
                                    show-meridian="true">
                                </uib-timepicker>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    <button type="button" class="btn btn-primary" ng-click="vm.insertData()" data-dismiss="modal"><i class="fa fa-save"></i> Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>