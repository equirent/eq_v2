<!DOCTYPE html>
<html ng-app="eQuiRent">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $this->title; ?></title>
        <!-- Header CSS files -->
        <?php foreach ($this->css_files as $file) : ?>
            <link rel="stylesheet" type="text/css" href="<?php echo $file; ?>">
        <?php endforeach; ?>
        <!-- Header JavaScript files -->
        <?php foreach ($this->js_files as $file) : ?>
            <script type="text/javascript" src="<?php echo $file ?>"></script>
        <?php endforeach; ?>

    </head>
    <body>
    <!-- START : body -->
