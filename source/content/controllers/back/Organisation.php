<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 3/4/2016
 * Time: 1:44 PM
 */

namespace content\controllers\back;

use core\controllers\PageBackController;

/**
 * Class Organisation
 * @package content\controllers\back
 */
class Organisation extends PageBackController
{

    /**
     *
     */
    protected function setMedia()
    {
        parent::setMedia();
        $this->addJS('back/resources/js/organisation.js', true);
    }

    /**
     * Display content
     */
    protected function displayContent()
    {
        $this->createTemplate("back/organisation.php", array(
            // Assign variables
        ))->show();
    }

}