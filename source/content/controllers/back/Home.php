<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 17-2-2016
 * Time: 13:13
 */

namespace content\controllers\back;

use core\controllers\PageBackController;

/**
 * Class Home
 * @package content\controllers\back
 */
class Home extends PageBackController
{

    /**
     *
     */
    protected function setMedia()
    {
        parent::setMedia();

        $this->addJS('resources/js/moment.min.js', true);
        $this->addJS('resources/js/angular-animate.min.js', true);

        $this->addCSS('resources/css/angular-bootstrap-calendar.css');
        $this->addJS('resources/js/angular-bootstrap-calendar-tpls.js', true);

        $this->addJS('back/resources/js/calendar.js', true);
        
    }

    /**
     * Display content
     */
    protected function displayContent()
    {
        $this->createTemplate('back/calendar.php', array(
            // Assign variables...
        ))->show();
    }

}