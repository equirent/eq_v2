<?php

namespace content\controllers\back;

use core\controllers\PageBackController;

/**
 * Class Workplace
 * @package content\controllers\back
 */
class Workplace extends PageBackController
{

    /**
     *
     */
    protected function setMedia()
    {
        parent::setMedia();
        $this->addJS('back/resources/js/workplace.js', true);
    }

    /**
     * Display content
     */
    protected function displayContent()
    {
        $this->createTemplate("back/workplace.php", array(
            // Assign variables
        ))->show();
    }

}