<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 7-3-2016
 * Time: 12:33
 */

namespace content\controllers\front;

use core\controllers\PageFrontController;

/**
 * Class Home
 * @package content\controllers\front
 */
class Home extends PageFrontController
{

    /**
     * Display content
     */
    protected function displayContent()
    {
        $this->createTemplate('front/home.php', array(
            // Assign variables..
        ))->show();
    }

}