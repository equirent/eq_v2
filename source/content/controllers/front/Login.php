<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 18-2-2016
 * Time: 11:44
 */

namespace content\controllers\front;

use core\classes\controller\ControllerHandler;
use core\classes\Tools;
use core\classes\User;
use core\controllers\PageFrontController;
use core\exceptions\InvalidControllerException;

/**
 * Class Login
 * @package content\controllers\front
 */
class Login extends PageFrontController
{

    /** Error types */
    const ERROR_WRONG_USER_INFO = 1;
    const ERROR_PERMISSIONS_DENIED = 2;
    const ERROR_SESSION_ENDED = 3;

    /** @var bool */
    protected $error_code = 0;

    /**
     * Login constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->title = 'Login';

    }

    /**
     * @param $error_code
     * @return bool|string
     */
    protected function getErrorMessage($error_code)
    {
        switch ($error_code) {
            case 1:
                return 'Gebruikersnaam of wachtwoord is incorrect.';
            case 2:
                return 'U moet ingelogd zijn voor deze pagina.';
            case 3:
                return 'Uw sessie was beëindigd.';
        }
        return false;
    }

    /**
     * Whether we have access to the controller
     * @return bool
     */
    public function hasAccess()
    {

        // Get the remembered user
        $user = User::getRemembered();

        // No access if the user is loaded
        return $user == false;
    }

    /**
     * Called when hasAccess returns false
     */
    protected function onAccessDenied()
    {

        // Redirect to the home back controller is we are logged in
        $this->redirect(ControllerHandler::getInstance()->getReference('a-home'));

    }

    /**
     *
     */
    protected function process()
    {
        $this->error_code = $this->getValue('error');
    }

    /**
     * Set media
     */
    protected function setMedia()
    {
        parent::setMedia();
        $this->addCSS('front/resources/css/login.css');
    }

    /**
     * Display the header content
     */
    protected function displayHeaderContent()
    {
        // No header content
    }

    /**
     * Display content
     */
    protected function displayContent()
    {

        $login_url = $this->reference->buildUrl();
        $login_url = Tools::appendQueryString($login_url, array(
            'method' => 'login',
            'ref' => $this->getValue('ref')
        ));

        $this->createTemplate('front/login.php', array(
            'login_url' => $login_url,
            'error_message' => $this->getErrorMessage($this->error_code)
        ))->show();
    }

    /**
     * Display the footer
     */
    protected function displayFooterContent()
    {
        // No footer content
    }

    /**
     * Called when the user attempts to log in
     */
    public function process_login()
    {

        // Post values
        $email = $this->getValue('email');
        $password = $this->getValue('password');
        $remember = (bool)$this->getValue('remember');

        // Get the user
        if ($user = User::getByEmailAndPassword($email, $password)) {

            if (!$user) {

                // Refresh page give error
                $this->refresh(array(
                    'error' => self::ERROR_WRONG_USER_INFO
                ));

            }

            // Remember the user on a long term
            $user->remember(!$remember);

            // Either redirect to given reference or the home page
            $reference = false;
            if (!empty($reference_identifier = $this->getValue('ref'))) {
                try {
                    $reference = ControllerHandler::getInstance()->getReference($reference_identifier);
                } catch (InvalidControllerException $e) {}
            }

            if (!$reference) {
                $reference = ControllerHandler::getInstance()->getReference('a-home');
            }

            // Redirect
            $this->redirect($reference);

        } else {

            // Refresh page give error
            $this->refresh(array(
                'error' => self::ERROR_WRONG_USER_INFO
            ));

        }

    }

    /**
     * Called when the user attempts to log out
     */
    public function process_pre_logout()
    {
        User::logout();
        $this->refresh(array(
            'error' => self::ERROR_SESSION_ENDED
        ));
    }

}