<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 14-3-2016
 * Time: 12:30
 */

namespace content\controllers\api;

use content\classes\Color;
use core\controllers\APIController;
use content\classes\Organisation as OrganisationModel;
use core\exceptions\InvalidValueException;
use core\exceptions\ResponseException;

/**
 * Class Organisation
 * @package content\controllers\api
 */
class Organisation extends APIController
{

    protected function process_getAll()
    {
        $data = &$this->response->getData();
        foreach (OrganisationModel::getAll() as $organisation) {
            $data[] = array(
                "id" => $organisation->id,
                "name" => $organisation->name,
                "location" => $organisation->location,
                "color" => $organisation->color->toArray(),
                "abbreviation" => $organisation->abbreviation
            );
        }
    }

    protected function process_insert()
    {

        $name = $this->getValue("org_name");
        $location = $this->getValue("org_location");
        $color = Color::stringToColor($this->getValue("org_color"));
        $abbreviation = $this->getValue("org_abbreviation");

        if (!$name || !$location || !$color || !$abbreviation) {
            throw new ResponseException("Invalid request data");
        }

        $organisation = new OrganisationModel();
        $organisation->name = $name;
        $organisation->location = $location;
        $organisation->color = $color;
        $organisation->abbreviation = $abbreviation;

        if (!$organisation->insert()) {
            throw new ResponseException("Failed to insert");
        }
    }

    protected function process_delete()
    {
        $id = $this->getValue("org_id");

        if ($id === false) {
            throw new ResponseException("Invalid request data");
        }

        $organisation = new OrganisationModel();
        $organisation->id = $id;
        $organisation->delete();

    }

    protected function process_update()
    {
        $id = $this->getValue("org_id");
        $name = $this->getValue("org_name");
        $location = $this->getValue("org_location");
        $color = Color::stringToColor($this->getValue("org_color"));
        $abbreviation = $this->getValue("org_abbreviation");

        if ($id === false || !$name || !$location || !$color || !$abbreviation) {
            throw new ResponseException("Invalid request data");
        }

        $organisation = new OrganisationModel();
        $organisation->id = $id;
        $organisation->name = $name;
        $organisation->location = $location;
        $organisation->color = $color;
        $organisation->abbreviation = $abbreviation;
        $organisation->update();
    }

}