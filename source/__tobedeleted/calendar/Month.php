<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 18-2-2016
 * Time: 11:13
 */

namespace core\classes\calendar;


final class Month extends DateSet
{

    /**
     * Month constructor.
     * @param \DateTime $date
     */
    public function __construct(\DateTime $date)
    {

        $date = clone $date;

        // Set the time to 00:00:00
        $date->setTime(0, 0, 0);

        // Set the first day of the month
        $date->modify('first day of this month');
        $this->startDate = clone $date;

        // Set the last day of the month
        $date->modify('last day of this month');
        $this->endDate = clone $date;

    }

    /**
     * Get the next month
     * @return Month
     */
    public function getNext()
    {
        $date = clone $this->startDate;
        $date->modify('first day of next month');
        return new Month($date);
    }

    /**
     * Get the previous month
     * @return Month
     */
    public function getPrevious()
    {
        $date = clone $this->startDate;
        $date->modify('first day of previous month');
        return new Month($date);
    }

    /**
     * Get the month number in the year
     * @return int
     */
    public function getNumber()
    {
        $date = clone $this->startDate;
        return (int)$date->format('n');
    }

}