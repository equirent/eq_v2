<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 18-2-2016
 * Time: 11:14
 */

namespace core\classes\calendar;


final class Day extends DateSet
{

    /**
     * Month constructor.
     * @param \DateTime $date
     */
    public function __construct(\DateTime $date)
    {

        $date = clone $date;

        // Set the time to 00:00:00
        $date->setTime(0, 0, 0);

    }

    /**
     * Get the next month
     * @return Month
     */
    public function getNext()
    {
        $date = clone $this->startDate;
        $date->modify('+1 day');
        return new Day($date);
    }

    /**
     * Get the previous month
     * @return Month
     */
    public function getPrevious()
    {
        $date = clone $this->startDate;
        $date->modify('-1 day');
        return new Day($date);
    }

    /**
     * Get the month number in the year
     * @return int
     */
    public function getNumber()
    {
        $date = clone $this->startDate;
        return (int)$date->format('z');
    }

}