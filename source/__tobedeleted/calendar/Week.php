<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 18-2-2016
 * Time: 11:13
 */

namespace core\classes\calendar;


final class Week extends DateSet {

    /**
     * Numeric representation of the day of the week
     * Compatible with DateTime format 'w'
     */
    const SUNDAY = 0;
    const MONDAY = 1;
    const TUESDAY = 2;
    const WEDNESDAY = 3;
    const THURSDAY = 4;
    const FRIDAY = 5;
    const SATURDAY = 6;

    /**
     * Week constructor.
     * @param \DateTime $date
     */
    public function __construct(\DateTime $date)
    {

        $date = clone $date;

        // Set the time to 00:00:00
        $date->setTime(0, 0, 0);

        // Set the first day of the week
        $date->modify('monday this week');
        $this->startDate = clone $date;

        // Set the last day of the week
        $date->modify('sunday this week');
        $this->endDate = clone $date;

    }

    /**
     * Get the previous week
     * @return Week
     */
    public function getNext()
    {
        $date = clone $this->startDate;
        $date->modify('+1 week');
        return new Week($date);
    }

    /**
     * Get the next week
     * @return Week
     */
    public function getPrevious()
    {
        $date = clone $this->startDate;
        $date->modify('-1 week');
        return new Week($date);
    }

    /**
     * Get the week number in the year
     * @return int
     */
    public function getNumber()
    {
        $date = clone $this->startDate;
        return (int)$date->format('W');
    }

}