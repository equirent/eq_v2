<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 18-2-2016
 * Time: 11:12
 */

namespace core\classes\calendar;


abstract class DateSet
{

    /** @var \DateTime */
    public $startDate;

    /** @var  \DateTime */
    public $endDate;

    /**
     * DateSet constructor.
     * @param \DateTime $date
     */
    public abstract function __construct(\DateTime $date);

    /**
     * @return mixed
     */
    public abstract function getNext();

    /**
     * @return mixed
     */
    public abstract function getPrevious();

}