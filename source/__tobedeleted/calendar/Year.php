<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 18-2-2016
 * Time: 11:34
 */

namespace core\classes\calendar;


final class Year extends DateSet
{

    /**
     * Year constructor.
     * @param \DateTime $date
     */
    public function __construct(\DateTime $date)
    {

        $date = clone $date;

        // Set the time to 00:00:00
        $date->setTime(0, 0, 0);

        // Set the first day of the year
        $date->modify('first day of this year');
        $this->startDate = clone $date;

        // Set the last day of the year
        $date->modify('last day of this year');
        $this->endDate = clone $date;

    }

    /**
     * Get the next month
     * @return Month
     */
    public function getNext()
    {
        $date = clone $this->startDate;
        $date->modify('first day of next year');
        return new Year($date);
    }

    /**
     * Get the previous month
     * @return Month
     */
    public function getPrevious()
    {
        $date = clone $this->startDate;
        $date->modify('first day of previous year');
        return new Year($date);
    }

    /**
     * Get the year number
     * @return int
     */
    public function getNumber()
    {
        $date = clone $this->startDate;
        return (int)$date->format('o');
    }

}