<?php
/**
 * Created by PhpStorm.
 * User: Armando
 * Date: 18-2-2016
 * Time: 09:05
 */

$oCalendar = new \core\classes\calendar\Calendar();
$oMonth = new \core\classes\calendar\Month(new DateTime());
$oWeek = new \core\classes\calendar\Week(new DateTime());

echo '<pre>';
echo PHP_EOL, $oMonth->startDate->format('Y-m-d H:i:s'), PHP_EOL,$oMonth->endDate->format('Y-m-d H:i:s');
echo PHP_EOL, $oMonth->getNext()->startDate->format('Y-m-d H:i:s');
echo PHP_EOL, $oMonth->getPrevious()->startDate->format('Y-m-d H:i:s');

echo PHP_EOL, $oWeek->startDate->format('Y-m-d H:i:s'), PHP_EOL,$oWeek->endDate->format('Y-m-d H:i:s');
echo PHP_EOL, $oWeek->getNext()->startDate->format('Y-m-d H:i:s');
echo PHP_EOL, $oWeek->getPrevious()->startDate->format('Y-m-d H:i:s');

echo '</pre>';