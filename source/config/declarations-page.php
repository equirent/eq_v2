<?php

use \core\classes\controller\ControllerHandler;
use \core\classes\controller\ControllerDeclaration;

// Get the controller handler
$handler = ControllerHandler::getInstance();

/**
 * Declare page controllers below
 *
 * All back controllers must start with 'a-' and '/a/'
 *
 */

$handler->register(new ControllerDeclaration('installation', 'Installation', function() {
    return '/installation/';
}));

$handler->register(new ControllerDeclaration('login', 'front\\Login', function() {
    return '/login/';
}));

$handler->register(new ControllerDeclaration('home', 'front\\Home', function() {
    return '/home/';
}));

$handler->register(new ControllerDeclaration('a-home', 'back\\Home', function() {
    return '/a/home/';
}));

$handler->register(new ControllerDeclaration('a-organisation', 'back\\Organisation', function() {
    return '/a/organisations/';
}));

$handler->register(new ControllerDeclaration('a-workplace', 'back\\WorkPlace', function() {
    return '/a/workplace/';
}));