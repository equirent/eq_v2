<?php

use core\classes\config\Defines;

// Framework's version
Defines::set('VERSION', '0.1.00');
Defines::set('VERSION_NR', 0100);

// Root directory
Defines::set('ROOT_DIR', dirname(__DIR__).'/');

$base_url = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://').$_SERVER['HTTP_HOST'];
Defines::set('ROOT_FILE_URL', $base_url.$_SERVER['PHP_SELF']);
Defines::set('ROOT_DIR_URL', dirname($base_url.$_SERVER['PHP_SELF']));

// Default time zone
date_default_timezone_set('Europe/Amsterdam');

ini_set('always_populate_raw_post_data', -1);