<?php

use \core\classes\controller\ControllerHandler;
use \core\classes\controller\ControllerDeclaration;

// Get the controller handler
$handler = ControllerHandler::getInstance();

/**
 * Declare api controllers below
 */

$handler->register(new ControllerDeclaration('api-organisation', 'api\\Organisation', function() {
    return '/api/organisation/';
}));