<?php

use core\classes\config\Settings;

// Whether we are in develop mode
Settings::set('DEVELOP_MODE', true);

// Database info
Settings::set('DATABASE_HOST', '127.0.0.1');
Settings::set('DATABASE_NAME', 'equirent');
Settings::set('DATABASE_USER', 'root');
Settings::set('DATABASE_PASS', '');

// Whether to use SEO friendly urls
Settings::set('SEO_URLS', false);

// Landing/home page reference identifier
Settings::set('LANDING_PAGE_IDENTIFIER', 'login');

// Encryption key used to encrypt
Settings::set('ENCRYPTION_KEY', '0123456789012345');